//[home-slider Javascript]

//Project:	Wine Store Html Responsive Template
//Version:	1.1
//Last change:	1/03/2018
//Primary use:	Wine Store Html Responsive Template 



jQuery(function ($) {
	
	"use strict";
	
// Masterslider 
	var slider = new MasterSlider();
	slider.setup('masterslider' , {
		width:1024,
		height:650,
		//space:100,
		fullwidth:true,
		centerControls:false,
		speed:18,
		view:'flow',
		overPause:false,
		autoplay:true
	});
	//slider.control('arrows');
	slider.control('bullets' ,{autohide:false}); 
	
	
// Portfolio and blog slider
	
	$('.product-slider').owlCarousel({
		loop:true,
		margin:15,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:false,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				nav:false
			},
			600:{
				items:3,
				nav:false
			},
			1000:{
				items:4,
				nav:true,
				dots:false
			}
		}

	});
	
// partnerlogo slider
	
	$('.partnerlogo-slide').owlCarousel({
		loop:true,
		margin:15,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:false,
		responsiveClass:true,
		responsive:{
			0:{
				items:3,
				nav:false
			},
			600:{
				items:4,
				nav:false
			},
			1000:{
				items:6,
				nav:true,
				dots:false
			}
		}

	});
	
});// End of use strict