//[product-slider Javascript]

//Project:	Wine Store Html Responsive Template
//Primary use:	Wine Store Html Responsive Template 



jQuery(function ($) {
    "use strict";
	
// Masterslider shop
	 var slider = new MasterSlider();
         
        slider.control('arrows');  
        slider.control('thumblist' , {autohide:false ,dir:'h',arrows:true, align:'bottom', width:127, height:137, margin:5, space:5});
     
        slider.setup('masterslidershop' , {
            width:540,
            height:586,
            space:5,
            view:'scale'
        });
	
});// End of use strict