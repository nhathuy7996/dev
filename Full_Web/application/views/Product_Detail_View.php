<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= BASE_URL().'public/'?>images/<?= $setting['icon'] ?>">

    <title><?= $Product_detail['name'] ?></title>

    <?php $this->load->view('fragments/Header.load.php'); ?>

   	<!-- Base MasterSlider style sheet -->
    <link rel="stylesheet" href="<?= BASE_URL().'public/' ?>vendor/masterslider/style/masterslider.css" />
     
    <!-- Master Slider Skin -->
    <link href="<?= BASE_URL().'public/' ?>vendor/masterslider/skins/black-1/style.css" rel="stylesheet" type="text/css" />
     
  </head>

  <body id="home">
  	
	<!-- loader start -->

	<div class="loader">
		<div id="awsload-pageloading">
			<div class="awsload-wrap">
				<ul class="awsload-divi">
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- loader end -->
    
	<?php $this->load->view('fragments/Topbar.fragment.php') ?>
    
    <!--Page Title-->
    
	<div class="page_title_ctn"> 
		<div class="container">
			<div class="row">                        
				<div class="col-sm-12">
					<div class="page-title clearfix">
						<h3>Shop</h3>
						<ol class="breadcrumb">
						  <li><a href="#">Home</a></li>
						  <li class="active"><span>Shop</span></li>
						</ol>
					</div>
				</div>
    		</div>           
    	</div>
    </div>  
    
		
    <!--Shoping with Sidebar Section-->
    <div class="shop-pages">
        <section class="product-single-wrap section-padding">
            <div class="container">
                <div class="product-content-wrap">
                    <div class="row">                        
                        <div class="col-md-6 col-sm-8 col-sm-offset-2 col-md-offset-0">
                            
                            <!-- template -->
                            <div class="ms-showcase2-template ms-showcase2-vertical">
								<!-- masterslider -->
								<div class="master-slider ms-skin-default" id="masterslidershop">
                                    <?php $image = json_decode($Product_detail['image']);
                                        foreach($image as $img){
                                    ?>
									<div class="ms-slide">
										<img src="<?= BASE_URL().'public/' ?>vendor/masterslider/style/blank.gif" 
                                        data-src="<?= BASE_URL().'public/' ?>images/product/details/<?= $Product_detail['id_product'] ?>/<?= $img ?>" 
                                        alt="<?= $Product_detail['id_product'] ?>"/> 
										<img class="ms-thumb" src="<?= BASE_URL().'public/' ?>images/product/details/<?= $Product_detail['id_product'] ?>/<?= $img ?>" alt="thumb" />
									</div>
                                        <?php } ?>
								</div>
								<!-- end of masterslider -->
                            </div>
                            <!-- end of template --> 
                            
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="product-content dart-mt-30">                                     
                                <div class="product-title">
                                    <h2><?= $Product_detail['name'] ?></h2> 
                                                                   
                                </div>
                                <div class="product-price-review">
                                    <span class="font-semibold black-color"><?= $Product_detail['price'] ?>VNĐ</span> 
                             
                                </div>
                                <div class="product-description">
                                    <?= $Product_detail['description'] ?>
                                    <div class="qty-add-wish-btn">
                                    <form id="form" method = "POST" >

                                        <span class="quantity">
                                            <input type="number" class="input-text qty text" title="Qty" min="1" value="1" name="number_product">
                                        </span>
                                        <span><input class="btn rd-stroke-btn border_2px dart-btn-sm" type="submit" form="form" value="Add To Cart"/></span>
                                        <!--<span><a class="btn rd-stroke-btn border_2px dart-btn-sm" href="#">Add To Wishlist</a></span> -->
                                    </form>
                                       
                                    <div class="product-title">
                                        <h3> Available: <?= $Product_detail['Quantity'] ?></h3>                               
                                    </div>

                                    </div>
                                    <div class="social-media">
                                        <span class="black-color">Share with friends</span>
                                        <ul class="social-icons list-unstyled">
                                            <li> <a href="#"> <span class="fa fa-facebook"></span> </a> </li>
                                            <li> <a href="#"> <span class="fa fa-twitter"></span> </a> </li>
                                            <li> <a href="#"> <span class="fa fa-pinterest"></span> </a> </li>
                                            <li> <a href="#"> <span class="fa fa-instagram"></span> </a> </li>
                                            <li> <a href="#"> <span class="fa fa-skype"></span> </a> </li>
                                            <li> <a href="#"> <span class="fa fa-dribbble"></span> </a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>        
    </section>
    </div>             
                   
	<?php $this->load->view('fragments/Footer.fragment.php') ?>   
	
	<!-- jQuery -->
    <!--<script src="vendor/masterslider/jquery.min.js"></script>-->
    <script src="<?= BASE_URL().'public/' ?>vendor/masterslider/jquery.easing.min.js"></script>
     
    <!-- Master Slider -->
    <script src="<?= BASE_URL().'public/' ?>vendor/masterslider/masterslider.min.js"></script>
    
    
    <!-- product-slider JavaScript -->
    <script src="<?= BASE_URL().'public/' ?>js/product-slider.js"></script>

  </body>
</html>
