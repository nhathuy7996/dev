<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= BASE_URL().'public/'?>images/<?= $setting['icon'] ?>">

    <title><?= $this->l('Privacy Policy') ?></title>

    <?php $this->load->view('fragments/Header.load.php') ?>

     
  </head>

  <body id="home">
  	
	<!-- loader start -->

	<div class="loader">
		<div id="awsload-pageloading">
			<div class="awsload-wrap">
				<ul class="awsload-divi">
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- loader end -->
    
    <?php $this->load->view('fragments/Topbar.fragment.php') ?>

    
    <!--Page Title-->
    
	<div class="page_title_ctn"> 
		<div class="container">
			<div class="row">                        
				<div class="col-sm-12">
					<div class="page-title clearfix">
						<h3><?= $this->l('Privacy Policy') ?></h3>
					</div>
				</div>
    		</div>           
    	</div>
    </div>
    
    <!-- Contact Section -->
    
    <section class="contactus-one" id="contactus"><!-- Section id-->
        <div class="container">
           <?= $info['policy'] ?>          
        </div>
    </section>            
                   
    <?php $this->load->view('fragments/Footer.fragment.php'); ?>

  </body>
</html>
