<?php
 $sql="SELECT * FROM orders WHERE status_order = 'pending' ORDER BY id_order DESC";
 $query = $this->db->query($sql);

 $orders = $query->result_array();

    $this->load->model('Setting_Model');
    $icon = $this->Setting_Model->Get_all_setting()['icon'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Huynn">
    <meta name="keywords" content="wine store">
    <link rel="icon" href="<?= BASE_URL().'public/' ?>images/<?= $icon ?>">
    <!-- Title Page-->
    <title>Dashboard</title>

    <!-- Fontfaces CSS-->
    <link href="<?= BASE_URL().'admin_public/' ?>css/font-face.css" rel="stylesheet" media="all">
    <link href="<?= BASE_URL().'admin_public/' ?>vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?= BASE_URL().'admin_public/' ?>vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?= BASE_URL().'admin_public/' ?>vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?= BASE_URL().'admin_public/' ?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?= BASE_URL().'admin_public/' ?>vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?= BASE_URL().'admin_public/' ?>vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?= BASE_URL().'admin_public/' ?>vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?= BASE_URL().'admin_public/' ?>vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?= BASE_URL().'admin_public/' ?>vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?= BASE_URL().'admin_public/' ?>vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?= BASE_URL().'admin_public/' ?>vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?= BASE_URL().'admin_public/' ?>css/theme.css" rel="stylesheet" media="all">
    <script src="<?= BASE_URL().'admin_public/' ?>ckeditor/ckeditor.js"></script>
</head>

<body class="">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="<?= BASE_URL().'Admin' ?>">
                            <img width="32" height="32" src="<?= BASE_URL().'public/' ?>images/<?= $icon ?>" alt="Cooladmin_public/" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-image"></i>IMAGES</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Slider' ?>">Slider</a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Promo' ?>">Promo</a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Logo' ?>">Logo</a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Partner' ?>"><?= $this->l("Partner Logo") ?></a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-cart-plus"></i>PRODUCTS</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                <a href="<?= BASE_URL().'Admin/Products' ?>">All Products</a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/MainCate' ?>">Main Categories</a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/SubCate' ?>">Sub Categories</a>
                                </li>                              
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>BLOGS</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Blog' ?>">All Blogs</a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/BlogTheme' ?>">Theme</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fa fa-cog"></i>SETTINGS</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Setting' ?>"><?= $this->l('Basic Settings') ?></a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Info' ?>"><?= $this->l('Infor Setting') ?></a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Condition' ?>"><?= $this->l('Rules Setting') ?></a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/HtmlInfor' ?>"><?= $this->l('Html Page') ?></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="<?= BASE_URL().'Admin' ?>">
                    <img width="32" height="32" src="<?= BASE_URL().'public/' ?>images/<?= $icon ?>" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-image"></i>IMAGES</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Slider' ?>">Slider</a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Promo' ?>">Promo</a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Logo' ?>">Logo</a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Partner' ?>"><?= $this->l("Partner Logo") ?></a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-cart-plus"></i>PRODUCTS</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Products' ?>">All Products</a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/MainCate' ?>">Main Categories</a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/SubCate' ?>">Sub Categories</a>
                                </li> 
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>BLOGS</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Blog' ?>">All Blogs</a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/BlogTheme' ?>">Theme</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-cog"></i>SETTINGS</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Setting' ?>"><?= $this->l('Basic Settings') ?></a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Info' ?>"><?= $this->l('Infor Setting') ?></a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/Condition' ?>"><?= $this->l('Rules Setting') ?></a>
                                </li>
                                <li>
                                    <a href="<?= BASE_URL().'Admin/HtmlInfor' ?>"><?= $this->l('Html Page') ?></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <div class="header-button" align="left">
                                <div class="noti-wrap">
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-notifications"></i>
                                        <?php if(!empty($orders) && count($orders) > 0) { ?>
                                        <span class="quantity"><?= count($orders) ?></span>
                                        <?php }?>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have <?= !empty($orders)? count($orders) : 0 ?> Orders</p>
                                            </div>
                                            <?php if(!empty($orders)) foreach($orders as $o) {?>
                                                <div class="notifi__item">
                                                <div class="bg-c1 img-cir img-40">
                                                    <i class="zmdi zmdi-file-text"></i>
                                                </div>
                                                <div class="content">
                                                    <p><?= $o['name'] ?></p>
                                                    <span class="date"><?= $o['date_create'] ?></span>
                                                </div>
                                            </div>
                                           
                                            <?php } ?>
                                            <div class="notifi__footer">
                                                <a href="<?= BASE_URL()."Admin/Orders" ?>">All Orders</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="<?= BASE_URL().'public/images/admin.png' ?>" alt="Admin" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#">Admin</a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="<?= BASE_URL().'public/images/admin.png' ?>" alt="Admin" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="<?= BASE_URL() ?>" target="_blank">Admin</a>
                                                    </h5>

                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">       
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-settings"></i>Setting</a>
                                                </div>
                                 
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="<?= BASE_URL().'Admin?Logout=true' ?>">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            
 