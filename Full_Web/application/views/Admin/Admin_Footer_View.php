           <!-- END PAGE CONTAINER-->
           </div>

</div>

<!-- Jquery JS-->
<script src="<?= BASE_URL().'admin_public/' ?>vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="<?= BASE_URL().'admin_public/' ?>vendor/bootstrap-4.1/popper.min.js"></script>
<script src="<?= BASE_URL().'admin_public/' ?>vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="<?= BASE_URL().'admin_public/' ?>vendor/slick/slick.min.js">
</script>
<script src="<?= BASE_URL().'admin_public/' ?>vendor/wow/wow.min.js"></script>
<script src="<?= BASE_URL().'admin_public/' ?>vendor/animsition/animsition.min.js"></script>
<script src="<?= BASE_URL().'admin_public/' ?>vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="<?= BASE_URL().'admin_public/' ?>vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="<?= BASE_URL().'admin_public/' ?>vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="<?= BASE_URL().'admin_public/' ?>vendor/circle-progress/circle-progress.min.js"></script>
<script src="<?= BASE_URL().'admin_public/' ?>vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="<?= BASE_URL().'admin_public/' ?>vendor/chartjs/Chart.bundle.min.js"></script>
<script src="<?= BASE_URL().'admin_public/' ?>vendor/select2/select2.min.js">
</script>

<!-- Main JS-->
<script src="<?= BASE_URL().'admin_public/' ?>js/main.js"></script>

</body>

</html>
<!-- end document-->
