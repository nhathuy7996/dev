<?php
 $query=$this->db->query("SELECT * FROM blog_theme order By id");
 $blogs_theme = $query->result_array();

?>
<div class="main-content">
<div class="card">
<ul class="nav nav-tabs">
	<li class="nav-item dropdown">
		<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Theme
			<span class="caret"></span>
		</a>
		<div class="dropdown-menu">
            <?php foreach($blogs_theme as $b){ ?>
			    <a class="dropdown-item" href="<?= BASE_URL().'Admin/Blog/'.$b['id'] ?>"><?= $b['name'] ?></a>
			<?php } ?>
		</div>
	</li>
</ul>  
                            
<div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <img class="card-img-top" src="<?= BASE_URL().'public/' ?>images/product/add.png" alt="Card image cap">
                        <a href="<?= BASE_URL().'Admin/Blog?Add' ?>">
                            <div class="card-body">
                                <h4 class="card-title mb-3">Add Blog</h4>
                            </div>
                        </a>
                    </div>
                </div> 
                <?php foreach($post as $p) {?>
                    <div class="col-md-4">
                        <div class="card">
                            <img class="card-img-top" src="<?= BASE_URL().'public/' ?>images/blog-post/<?= $p['picture'] ?>" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title mb-3"><?= $p['title']?></h4>
                                <ul class="list-unstyled">
                                    <li><span class="author"> By: <a href="#"><?= $p['author'] ?></a></span></li>
                                    <li><span class="posted-date"><a href="#"><?= $p['date_create'] ?></a></span></li>
                                </ul>
                                <p class="card-text">
                                    <?= $p['summary'] ?> ...
                                </p>
                                <li><a class="pull-right" href="<?= BASE_URL().'Admin/Blog?id='.$p['id_post'] ?>"> More <i class="fa fa-angle-double-right"></i></a></li>
                            </div>
                        </div>
                    </div>       
                <?php } ?>
            </div><!-- /.row -->
                            <!-- Phân trang-->
                            <div align="center">
                                <a href="<?=  BASE_URL().'Admin/Blog/'?><?= !empty($theme['id']) ? $theme['id'].'/' : '' ?>Page/<?php if($page > 1) echo $page-1; else echo $page; ?>">
                                    <button type="button" class="btn btn-outline-primary">Pre</button>
                                </a>

                                <?php for($i = 0; $i<$pages_number; $i++){ 
                                            if($i+1 == $page) {	?>
                                                <a href="<?=  BASE_URL().'Admin/Blog/'?><?= !empty($theme['id']) ? $theme['id'].'/' : '' ?>Page/<?= $i+1 ?>">
                                                    <button type="button" class="btn btn-primary"><?= $i+1 ?></button>
                                                </a>
												
											<?php } else { ?>
                                                <a href="<?=  BASE_URL().'Admin/Blog/'?><?= !empty($theme['id']) ? $theme['id'].'/' : '' ?>Page/<?= $i+1 ?>">
                                                    <button type="button" class="btn btn-outline-primary"><?= $i+1 ?></button>
                                                </a>						
									<?php } }   ?>	

                                <a href="<?=  BASE_URL().'Admin/Blog/'?><?= !empty($theme['id']) ? $theme['id'].'/' : '' ?>Page/<?php if($page < $pages_number) echo $page+1; else echo $page; ?>">
                                    <button type="button" class="btn btn-outline-primary">Next</button>
                                </a>
                            </div>

                <!-- Phân trang-->
</div>
</div>