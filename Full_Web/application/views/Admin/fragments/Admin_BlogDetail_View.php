<div class="main-content">
    <div class="card">
        <div class="card-header">
            <strong>BLOG EDITOR</strong>
        </div>
        <div class="card-body card-block">
            <form id="editBlog" method="post" enctype="multipart/form-data" class="form-horizontal" >
                <div class="col col-md-3">
                    <?php if(isset($post['picture'])){ ?>
                    <img class="card-img-top" src="<?= BASE_URL().'public/' ?>images/blog-post/<?=  $post['picture']  ?>" alt="Card image cap">
                    <?php } ?>
                </div>
                
                <input type="text" hidden id="id-post" name="id-post"  value="<?= isset ($post['id_post']) ? $post['id_post']:'' ?>">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="file-input" class=" form-control-label">New Icon</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="file" id="file-input" name="file-input" class="form-control-file">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="title" class=" form-control-label">Title</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="title" name="title" placeholder="Title" class="form-control" value="<?=isset ($post['title']) ? $post['title']:''  ?>">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="summary" class=" form-control-label">Summary</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="summary" name="summary" placeholder="Summary" class="form-control" value="<?= isset ($post['summary']) ? $post['summary']:''?>" >
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="author" class=" form-control-label">Author</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="author" name="author" placeholder="Author" class="form-control" value="<?= isset ($post['author']) ? $post['author']:'' ?>">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="content-blog" class=" form-control-label">Textarea</label>
                    </div>
                    <div class="col-12 col-md-9" >
                        <textarea name="content-blog" id="content-blog" rows="9" placeholder="Content..." class="ckeditor"><?= isset ($post['content']) ? $post['content']:'' ?></textarea>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="theme" class=" form-control-label">Theme</label>
                    </div>
                    <div class="col col-md-9">
                        <select name="theme[]" id="theme" multiple="" class="form-control">
                            <?php foreach($blogs_theme as $b){ ?>
                                <option value="<?= $b['id'] ?>"><?= $b['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-footer">
            <button form="editBlog" type="submit" class="btn btn-primary btn-sm">
                <i class="fa fa-dot-circle-o"></i> <?= isset($post) ? "Save" : "Upload" ?>
            </button>
            <?php if( isset($post['id_post'])) { ?>
            <a href="<?= BASE_URL().'Admin/Blog?Del='.$post['id_post'] ?>">
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Delete
                </button>
            </a>
            <?php } else { ?>
                <a href="<?= BASE_URL().'Admin/Blog'?>">
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Cancel
                </button>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
