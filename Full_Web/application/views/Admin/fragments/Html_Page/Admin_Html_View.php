<div class="main-content">
<div class = "card">
    <div class="card-body card-block">
        <?php foreach($body as $b){ ?>
        <form action="<?= BASE_URL().'Admin/HtmlInfor'?>" id="information" method="post"  class="form-horizontal" > 
            <input type="text" name="EditHtml" value="<?= $b['id'] ?>" hidden>        
            <div class="row form-group">
                    <div class="col col-md-3">
                        <input type="text" name="name" placeholder="<?= $this->l("name page") ?>" value="<?= $this->l($b['name']) ?>">
                    </div>
                    <div class="col-12 col-md-9" >
                        <textarea name="body" id="body" rows="9"  class="ckeditor"><?= isset ($b['content']) ? $b['content']:'' ?></textarea>
                    </div>
            </div>  
            <input type="submit" class="btn btn-primary btn-sm" value="<?= $this->l('Save') ?>" >
        </form>
        <?php } ?>
        <form action="<?= BASE_URL().'Admin/HtmlInfor'?>" id="information" method="post"  class="form-horizontal" > 
            <input type="text" name="AddHtml" value="" hidden>        
            <div class="row form-group">
                    <div class="col col-md-3">
                        <input type="text" name="name" placeholder="<?= $this->l("name page") ?>" value="">
                    </div>
                    <div class="col-12 col-md-9" >
                        <textarea name="body" id="body" rows="9"  class="ckeditor"></textarea>
                    </div>
            </div>  
            <input type="submit" class="btn btn-primary btn-sm" value="<?= $this->l('Add') ?>" >
        </form>
    </div>
</div>
</div>