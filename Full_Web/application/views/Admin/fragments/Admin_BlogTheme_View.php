<div class="main-content">
    <div class="card">
        <br>
        <form method = "post" class="form-horizontal">
            <div class="col-12 row form-group">
                <div class="col col-md-3">
                    <label for="new_theme" class=" form-control-label">New Theme</label>
                </div>
                <div class="col-12 col-md-6">
                    <input type="text" id="new_theme" name="new_theme" placeholder="name cate" class="form-control" >
                </div>
                <div class="col-12 col-md-3">
                    <input  class="btn btn-primary btn-sm" type="submit" value="Add">
                </div>
            </div>      
            <form>
       
        <?php foreach($theme as $b){ ?>
           
            <div class="col-12 row form-group">
                <input hidden type="text" id="id_theme" name  ="id_theme" value = "<?= $b['id'] ?>">
                <div class="col-12 col-md-6">
                    <input type="text" id="name_theme<?= $b['id'] ?>" name="name_theme" value="<?= $b['name'] ?>" class="form-control" >
                </div>
                <div class="col-12 col-md-3">
                    <button onclick="Edit(<?=  $b['id'] ?>)"  class="btn btn-outline-primary" >Edit</button>
                </div>
                <a  class="col-12 col-md-3" href="<?= BASE_URL().'Admin/BlogTheme?Del='.$b['id'] ?>">             
                    <button type="button" class="btn btn-danger btn-sm">Delete</button>
                </a>
            </div>    
            
        <?php } ?>
        <br>
    </div>
</div>
<script>

function Edit(id){
    var name = "#name_theme" + id;
    var value = $(name).val();
    $.post("<?= BASE_URL() ?>Admin/BlogTheme",
    {
        id_theme: id,
        name_theme: value
    },
    function(data, status){
        alert("Edited!");
    });
}

</script>
