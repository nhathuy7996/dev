<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <?php foreach($link_slide as $l){ ?>
                <img class="card-img-top" src="<?= BASE_URL().'public/' ?>images/slide/<?= $l['link'] ?>" alt="<?= $l['alt'] ?>"/>
                <br>
                    <div align="center">
                        <!--<button type="button" class="btn btn-warning">Down</button>-->
                        <a href="<?= BASE_URL().'Admin/Slider?id='.$l['id_slide'] ?>"><button type="button" class="btn btn-danger" >Delete</button></a>
                    </div>
                <br>
            <?php } ?>
        </div>
    </div>
    <div class="card">
        <div class="card-body card-block">
            <form id="editBlog" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="file-input" class=" form-control-label">Upload Slider</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="file" id="file-input" name="file-input" class="form-control-file">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="setting" class=" form-control-label"><?= $this->l("Setting Slider") ?></label>
                    </div>
                    <div class="col-12 col-md-9">
                    <textarea name="setting" id="setting" rows="9" placeholder="setting..." class="ckeditor"></textarea>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-footer">
            <button form="editBlog" type="submit" class="btn btn-primary btn-sm">
                <i class="fa fa-dot-circle-o"></i> Upload
            </button>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT-->