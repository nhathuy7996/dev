<div class="main-content">
<div class="row">
                        <div class="col-md-12">
                            <h3 hidden class="title-5 m-b-35">Order table</h3>
                            
                            <?php foreach($orders as $o){  ?>
                            <div hidden id="detail_<?= $o['id_order'] ?>" >
                                <table class="table table-data2">
                                    <thead>
                                        <tr>
                                            <th class="cart-product-thumbnail">Product</th>
                                            <th class="cart-product-quantity">Quantity</th>
                                            <th class="cart-product-subtotal">Total</th>
                                        </tr>
                                    </thead>
                                  
                                    <tbody>
                                       <?php
                                              $cart = json_decode($o['cart']);
                                              $total = 0;
                                              foreach($cart as $c){
                                                $total += $c->price * $c->quantity;
                                              
                                              ?>
                                        <tr>
                                            <td><?= $c->name ?></td>
                                            <td><?= $c->quantity ?></td>
                                            <td><?= $c->price * $c->quantity ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>

                                </table>
                            </div>
                            <?php } ?>

                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2">
                                    <thead>
                                        <tr>
                                            <th>name</th>
                                            <th>phone number</th>
                                            <th>date</th>
                                            <th>status</th>
                                            <th>total</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($orders as $o){  ?>
                                        <tr class="tr-shadow">
                                            <td><?= $o['name'] ?> <?= $o['last_name'] ?></td>
                                            <td>
                                                <span class="block-email"><?= $o['comp_name'] ?></span>
                                            </td>
                                            <td><?= $o['date_create'] ?></td>
                                            <td>
                                                <span class="status--<?= $o['status_order'] == 'pending'? 'denied':'process' ?>"><?= $o['status_order'] ?></span>
                                            </td>
                                            <td><?php
                                              $cart = json_decode($o['cart']);
                                              $total = 0;
                                              foreach($cart as $c){
                                                $total += $c->price * $c->quantity;
                                              }
                                              echo $total." VND";
                                              ?></td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <button onclick="Delivery(<?= $o['id_order'] ?>)" class="item" data-toggle="tooltip" data-placement="top" title="Delivery">
                                                        <i class="zmdi zmdi-mail-send"></i>
                                                    </button>
                                                    <button onclick="Detail(<?= $o['id_order'] ?>)" class="item" data-toggle="tooltip" data-placement="top" title="More">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </button>
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </button>
                                                    <button onclick="Done(<?= $o['id_order'] ?>)" class="item" data-toggle="tooltip" data-placement="top" title="Done">
                                                        <i class="zmdi zmdi-check"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="spacer"></tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
</div>
<script>
function Detail(id){
    $('[id^=detail_]').attr("hidden",true);
var id_order = "#detail_" + id;
    $(id_order).attr("hidden",false);
}

function Done(id){
    $.post("<?= BASE_URL() ?>Admin/Orders",
    {
        id_done: id
    },
    function(data, status){
        alert("Done!");
        location.reload();
    });
}

function Delivery(id){
    $.post("<?= BASE_URL() ?>Admin/Orders",
    {
        id_Delivery: id
    },
    function(data, status){
        alert("Delivery!");
        location.reload();
    });
}

</script>