<div class="main-content">
<div class = "card">
    <div class="card-body card-block">
        <form id="setting" method="post" enctype="multipart/form-data" class="form-horizontal" >
            <div class="row form-group">
                <input type="text" hidden name="Edit" class="form-control-file">
                <div class="col col-md-3">
                    <img id="logo_preview" class = "thumb" 
                        src="<?= BASE_URL()."public/images/" ?><?= isset($setting['logo']) ? $setting['logo'] : '' ?>">
                </div>
                <div class="col-12 col-md-9">
                    <input type="file" id="logo_image" name="logo_image" class="form-control-file">
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        <button form="setting" type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> <?= $this->l('Save') ?>
        </button>
        <a href="<?= BASE_URL().'Admin'?>">
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> <?= $this->l('Cancel') ?>
        </button>
        </a>
    </div>
</div>
</div>
<script>
function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

    // Only process image files.
    if (!f.type.match('image.*')) {
        continue;
    }

    var reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = (function(theFile) {
        return function(e) {
        // Render thumbnail.
        document.getElementById('logo_preview').setAttribute("src", e.target.result);
        };
    })(f);

    // Read in the image file as a data URL.
    reader.readAsDataURL(f);
    }
}

document.getElementById('logo_image').addEventListener('change', handleFileSelect, false);

</script>