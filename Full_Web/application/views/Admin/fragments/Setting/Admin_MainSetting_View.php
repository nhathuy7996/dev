<div class="main-content">
<div class = "card">
    <div class="card-body card-block">
        <form id="setting" method="post" enctype="multipart/form-data" class="form-horizontal" >
            <div class="row form-group">
                <input type="text" hidden name="Edit" class="form-control-file">
                <div class="col col-md-3">
                    <img id="icon_preview" class = "thumb" 
                        src="<?= BASE_URL()."public/images/" ?><?= isset($setting['icon']) ? $setting['icon'] : '' ?>">
                </div>
                <div class="col-12 col-md-9">
                    <input type="file" id="icon_image" name="icon_image" class="form-control-file">
                </div>
            </div>
            <div class="row form-group">
                <div class="col col-md-3">
                    <label for="name_web" class=" form-control-label"><?= $this->l('Web Name') ?></label>
                </div>
                <div class="col-12 col-md-9">
                    <input type="text" id="name_web" name="name_web" placeholder="<?= $this->l('Web Name') ?>" 
                        class="form-control" value="<?=isset ($setting['name_web']) ? $setting['name_web']:''  ?>">
                </div>
            </div>
            <div class="row form-group">
                <div class="col col-md-3">
                    <label for="slogan" class=" form-control-label"><?= $this->l('Slogan') ?></label>
                </div>
                <div class="col-12 col-md-9">
                    <input type="text" id="slogan" name="slogan" placeholder="<?= $this->l('Slogan') ?>" 
                        class="form-control" value="<?=isset ($setting['slogan']) ? $setting['slogan']:''  ?>">
                </div>
            </div>
            <div class="row form-group">
                <div class="col col-md-3">
                    <label for="intro" class=" form-control-label"><?= $this->l('Intro') ?></label>
                </div>
                <div class="col-12 col-md-9" >
                    <textarea name="intro" id="intro" rows="9" placeholder="Intro..." class="ckeditor"><?= isset ($setting['intro']) ? $setting['intro']:''  ?></textarea>
                </div>
            </div>
            <div class="row form-group">
                <div class="col col-md-3">
                    <label for="intro_image" class=" form-control-label"><?= $this->l('Intro Image') ?>(1920 x 779)</label>
                </div>
                <div class="col col-md-3">
                    <img id="intro_preview" class = "thumb" alt="intro image"
                        src="<?= BASE_URL()."public/images/" ?><?= isset($setting['intro_image']) ? $setting['intro_image'] : '' ?>">
                </div>
                <div class="col-12 col-md-9">
                    <input type="file" id="image_intro" name="image_intro" class="form-control-file">
                </div>
            </div>
            <div class="row form-group">
                <div class="col col-md-3">
                    <label for="Language" class=" form-control-label"><?= $this->l('Language') ?></label>
                </div>
                <div class="col-12 col-md-3" >
                    <input type="radio" name="language" value="vi"
                        <?= (isset($setting['language'] ) && $setting['language'] == "vi" )? 'checked' :'' ?>
                        ><?= $this->l('Vietnamese') ?><br>
                </div>
                <div class="col-12 col-md-3" >
                    <input type="radio" name="language" value="en"
                        <?= (isset($setting['language'] ) && $setting['language'] == "en" )? 'checked' :'' ?>
                        ><?= $this->l('English') ?><br>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        <button form="setting" type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> <?= $this->l('Save') ?>
        </button>
        <a href="<?= BASE_URL().'Admin'?>">
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> <?= $this->l('Cancel') ?>
        </button>
        </a>
    </div>
</div>
</div>
<script>
function handleFileSelectIcon(evt) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

    // Only process image files.
    if (!f.type.match('image.*')) {
        continue;
    }

    var reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = (function(theFile) {
        return function(e) {
        // Render thumbnail.
        document.getElementById('icon_preview').setAttribute("src", e.target.result);
        };
    })(f);

    // Read in the image file as a data URL.
    reader.readAsDataURL(f);
    }
}
function handleFileSelectIntro(evt) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

    // Only process image files.
    if (!f.type.match('image.*')) {
        continue;
    }

    var reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = (function(theFile) {
        return function(e) {
        // Render thumbnail.
        document.getElementById('intro_preview').setAttribute("src", e.target.result);
        };
    })(f);

    // Read in the image file as a data URL.
    reader.readAsDataURL(f);
    }
}

document.getElementById('icon_image').addEventListener('change', handleFileSelectIcon, false);
document.getElementById('image_intro').addEventListener('change', handleFileSelectIntro, false);
</script>