<div class="main-content">
<div class = "card">
    <div class="card-body card-block">
        <form id="information" method="post"  class="form-horizontal" > 
            <input hidden type="text" name="EditInfo" >
            <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="policy" class=" form-control-label"><?= $this->l('Privacy & Policy') ?></label>
                    </div>
                    <div class="col-12 col-md-9" >
                        <textarea name="policy" id="policy" rows="9" placeholder="<?= $this->l('Privacy & Policy') ?>..." class="ckeditor"><?= isset ($info['policy']) ? $info['policy']:'' ?></textarea>
                    </div>
            </div>  
            <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="term" class=" form-control-label"><?= $this->l('Term & Condition') ?></label>
                    </div>
                    <div class="col-12 col-md-9" >
                        <textarea name="term" id="term" rows="9" placeholder="<?= $this->l('Term & Condition') ?>..." class="ckeditor"><?= isset ($info['term']) ? $info['term']:'' ?></textarea>
                    </div>
            </div>         
        </form>
    </div>
    <div class="card-footer">
        <button form="information" type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> <?= $this->l('Save') ?>
        </button>
        <a href="<?= BASE_URL().'Admin'?>">
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> <?= $this->l('Cancel') ?>
        </button>
        </a>
    </div>
</div>
</div>