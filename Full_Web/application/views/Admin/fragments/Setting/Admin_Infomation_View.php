<div class="main-content">
<div class = "card">
    <div class="card-body card-block">
        <form id="information" method="post"  class="form-horizontal" > 
            <input hidden type="text" name="EditInfo" >
            <div class="row form-group">
                <div class="col col-md-3">
                    <label for="address" class=" form-control-label"><?= $this->l('Address') ?></label>
                </div>
                <div class="col-12 col-md-9">
                    <input type="text" id="address" name="address" placeholder="<?= $this->l('Address') ?>" 
                        class="form-control" value="<?=isset ($info['address']) ? $info['address']:''  ?>">
                </div>
            </div>  
            <div class="row form-group">
                <div class="col col-md-3">
                    <label for="phone" class=" form-control-label"><?= $this->l('Phone') ?></label>
                </div>
                <div class="col-12 col-md-9">
                    <input type="text" id="phone" name="phone" placeholder="<?= $this->l('Phone') ?>" 
                        class="form-control" value="<?=isset ($info['phone']) ? $info['phone']:''  ?>">
                </div>
            </div>
            <div class="row form-group">
                <div class="col col-md-3">
                    <label for="email" class=" form-control-label"><?= $this->l('Email') ?></label>
                </div>
                <div class="col-12 col-md-9">
                    <input type="text" id="email" name="email" placeholder="<?= $this->l('Email') ?>" 
                        class="form-control" value="<?=isset ($info['email']) ? $info['email']:''  ?>">
                </div>
            </div>  
            <div class="row form-group">
                <div class="col col-md-3">
                    <label for="shipping" class=" form-control-label"><?= $this->l('Shipping') ?></label>
                </div>
                <div class="col-12 col-md-9">
                    <input type="text" id="shipping" name="shipping" placeholder="<?= $this->l('Shipping') ?>" 
                        class="form-control" value="<?=isset ($info['shipping']) ? $info['shipping']:''  ?>">
                </div>
            </div>
            <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="about" class=" form-control-label"><?= $this->l('About') ?></label>
                    </div>
                    <div class="col-12 col-md-9" >
                        <textarea name="about" id="about" rows="9" placeholder="<?= $this->l('About us') ?>..." class="ckeditor"><?= isset ($info['about']) ? $info['about']:'' ?></textarea>
                    </div>
            </div>       
        </form>
    </div>
    <div class="card-footer">
        <button form="information" type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> <?= $this->l('Save') ?>
        </button>
        <a href="<?= BASE_URL().'Admin'?>">
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> <?= $this->l('Cancel') ?>
        </button>
        </a>
    </div>
</div>
</div>