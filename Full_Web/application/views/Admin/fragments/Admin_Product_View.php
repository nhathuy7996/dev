
<div class="main-content">
<div class="card">
    <div class = "row col-12 col-md-9">
        <?php foreach($main_cate as $b){ ?>
        <ul class="nav nav-tabs">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= $b['name'] ?>
                    <span class="caret"></span>
                </a>
                <div class="dropdown-menu">
                    <?php foreach($sub_cate as $s){  if($s['id_main_cate'] == $b['id_cate']) {?>
                        <a class="dropdown-item" href="<?= BASE_URL().'Admin/Products/'.$s['id_sub_cate'] ?>">
                        <?= $s['name'] ?>
                        </a>
                    <?php } } ?>
                </div>
            </li>
        </ul>
        <?php } ?>
    </div>
    <br>                                   
<div class="row">
                   
                    <div class="col-md-4">
                        <div class="card">
                            <img class="card-img-top" src="<?= BASE_URL().'public/' ?>images/product/add.png" alt="add products">
                            <a href="<?= BASE_URL().'Admin/Products?id=add' ?>">
                                <div class="card-body">
                                    <h4 class="card-title mb-3">Add Product</h4>
                                </div>
                            </a>
                        </div>
                    </div> 
                    
                <?php foreach($Products as $p) {?>
                    <div class="col-md-4">
                        <div class="card">
                            <img class="card-img-top" src="<?= BASE_URL().'public/' ?>images/product/details/<?= $p['id_product'] ?>/<?= isset(json_decode( $p['image'])[0]) ? json_decode( $p['image'])[0] : '' ?>" alt="<?= $p['name'] ?>">
                            <div class="card-body">
                                <h4 class="card-title mb-3"><?= $p['name']?></h4>
                                <ul class="list-unstyled">
                                    <li><span class="author"><a >New: <?= $p['sell_price'] ?> VNĐ</a></span></li>
                                    <?php if($p['price'] != $p['sell_price']) { ?>
                                        <li><span class="posted-date"><a >Old: <?= $p['price'] ?> VNĐ</a></span></li>
                                    <?php } ?>
                                
                                </ul>
                                <li><a class="pull-right" href="<?= BASE_URL().'Admin/Products?id='.$p['id_product'] ?>"> More <i class="fa fa-angle-double-right"></i></a></li>
                            </div>
                        </div>
                    </div>       
                <?php } ?>
            </div><!-- /.row -->
                            <!-- Phân trang-->
                            <div align="center">
                                <a href="<?=  BASE_URL().'Admin/Products/'?><?= !empty($catalog) ? $catalog.'/' : ''?>Page/<?php if($page > 1) echo $page-1; else echo $page; ?>">
                                    <button type="button" class="btn btn-outline-primary">Pre</button>
                                </a>

                                <?php for($i = 0; $i<$pages_number; $i++){ 
                                            if($i+1 == $page) {	?>
                                                <a href="<?=  BASE_URL().'Admin/Products/'?><?= !empty($catalog) ? $catalog.'/' : ''?>Page/<?= $i+1 ?>">
                                                    <button type="button" class="btn btn-primary"><?= $i+1 ?></button>
                                                </a>
												
											<?php } else { ?>
                                                <a href="<?=  BASE_URL().'Admin/Products/'?><?= !empty($catalog) ? $catalog.'/' : ''?>Page/<?= $i+1 ?>">
                                                    <button type="button" class="btn btn-outline-primary"><?= $i+1 ?></button>
                                                </a>						
									<?php } }   ?>	

                                <a href="<?=  BASE_URL().'Admin/Products/'?><?= !empty($catalog) ? $catalog.'/' : ''?>Page/<?php if($page < $pages_number) echo $page+1; else echo $page; ?>">
                                    <button type="button" class="btn btn-outline-primary">Next</button>
                                </a>
                            </div>

                <!-- Phân trang-->
</div>
</div>
