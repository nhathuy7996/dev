<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <?php foreach($link_promo as $l){ ?>
                <img class="card-img-top" src="<?= BASE_URL().'public/' ?>images/promo/<?= $l['link'] ?>" alt="<?= $l['alt'] ?>"/>
                <br>
                    <div align="center">
                        <!--<button type="button" class="btn btn-warning">Down</button>-->
                        <a href="<?= BASE_URL().'Admin/Promo?id='.$l['id'] ?>"><button type="button" class="btn btn-danger" >Delete</button></a>
                    </div>
                <br>
            <?php } ?>
        </div>
    </div>
    <div class="card">
        <div class="card-body card-block">
            <form id="uploadPromo" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="file-input" class=" form-control-label">Upload Promotion Image</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="file" id="file-input" name="file-input" class="form-control-file">
                    </div>
                </div>
            </form>
        </div>
        <div class="card-footer">
            <button form="uploadPromo" type="submit" class="btn btn-primary btn-sm">
                <i class="fa fa-dot-circle-o"></i> Upload
            </button>
        </div>
    </div>
</div>
<!-- END MAIN CONTENT-->