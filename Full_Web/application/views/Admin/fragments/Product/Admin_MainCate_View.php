<div class="main-content">
    <div class="card">
        <br>
        <form method = "post" class="form-horizontal">
            <div class="col-12 row form-group">
                <div class="col col-md-3">
                    <label for="new_cate" class=" form-control-label">New Cate</label>
                </div>
                <div class="col-12 col-md-6">
                    <input type="text" id="new_cate" name="new_cate" placeholder="name cate" class="form-control" >
                </div>
                <div class="col-12 col-md-3">
                    <input  class="btn btn-primary btn-sm" type="submit" value="Add">
                </div>
            </div>      
            <form>
       
        <?php foreach($main_cate as $b){ ?>
            <div class="col-12 row form-group">         
                <div class="col-12 col-md-6">
                    <input type="text"  id="name_cate<?= $b['id_cate'] ?>" value="<?= $b['name'] ?>" class="form-control" >
                </div>
                <div class="col-12 col-md-3">
                    <button  class="btn btn-outline-primary" onclick="Edit(<?= $b['id_cate'] ?>)" >Edit</button>
                </div>
                <a  class="col-12 col-md-3" href="<?= BASE_URL().'Admin/MainCate?Del='.$b['id_cate'] ?>">             
                    <button type="button" class="btn btn-danger btn-sm">Delete</button>
                </a>
            </div>    
        <?php } ?>
        <br>
    </div>
</div>
<script>
function Edit(id){
    var name = "#name_cate" + id;
    var value = $(name).val();

    $.post("<?= BASE_URL() ?>Admin/MainCate",
    {
        cate_id: id,
        name_cate: value
    },
    function(data, status){
        alert("Edited!");
    });
}
</script>