<div class="main-content">
    <div class="card">
        <div class="card-header">
            <strong>EDIT PRODUCT</strong>
        </div>
        <?php if(!empty($product)) { ?>
        <div class="card-body card-block">
            <form id="UploadImage" method = "post" enctype="multipart/form-data" class="form-horizontal">
                <div class="row form-group">
                    <input type="text" hidden id="id_product" name="id_product"  value="<?= isset ($product['id_product']) ? $product['id_product']:'' ?>">
                    <div class="col col-md-3">
                        <label class=" form-control-label">Upload Image Product</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="file" id="file-input" name="file-input" class="form-control-file">
                    </div>
                    <div class="col-12 col-md-9">
                        <input  class="btn btn-primary btn-sm" type="submit" value="Upload">
                    </div>        
                </div>
            <form>
            <form id="editProduct" method="post"  class="form-horizontal" >
                <div class = "row">
                    <?php if(!empty($product['image'])){ 
                                $image = json_decode($product['image']);
                                            foreach($image as $img){
                                        ?>
                    <ul class="col-md-4"> 
                        <img  src="<?= BASE_URL().'public/' ?>images/product/details/<?= $product['id_product'] ?>/<?= $img ?>" alt="Card image cap">    
                        <a href="<?= BASE_URL().'Admin/Products?id='.$product['id_product'].'&img='.$img ?>"><button type="button" class="btn btn-danger" >Delete</button></a>
                    </ul>
                    <?php } } ?>
                </div>
                <br>
                <input type="text" hidden id="id_product" name="id_product"  value="<?= isset ($product['id_product']) ? $product['id_product']:'' ?>">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="name_product" class=" form-control-label">Name</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="name_product" name="name_product" placeholder="name product" class="form-control" value="<?=isset ($product['name']) ? $product['name']:''  ?>">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="Catalogs" class=" form-control-label">Categories</label>
                    </div>
                    <div class="col col-md-3">
                        
                        <?php foreach($main_cate as $m){ ?>
                            <input type="button" onclick="Select_cate(<?= $m['id_cate'] ?>)" class="btn btn-primary btn-sm"  value="<?= $m['name'] ?>">
                        <?php } ?>
                       
                    </div>
                    <div class="col col-md-6">
                        <select name="Catalogs[]" id="Catalogs" multiple="" class="form-control">
                            <?php foreach($sub_cate as $b){ ?>
                                <option id="subcate<?= $b['id_main_cate'] ?>" value="<?= $b['id_sub_cate'] ?>"><?= $b['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <div class="row form-group">
                    <div class="col col-md-2">
                        <label for="name_product" class=" form-control-label"><?= $this->l("Price") ?></label>
                    </div>
                    <div class="col col-md-4">
                        <input type="text" id="price" name="price" placeholder="price" class="form-control" value="<?=isset ($product['price']) ? $product['price']:''  ?>">
                    </div>
                    <div class="col col-md-2">
                        <label for="name_product" class=" form-control-label"><?= $this->l("Sale Price") ?></label>
                    </div>
                    <div class="col col-md-4">
                        <input type="text" id="sell_price" name="sell_price" placeholder="<?= $this->l("Sale Price") ?>" class="form-control" value="<?=isset ($product['sell_price']) ? $product['sell_price']:''  ?>">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-2">
                        <label for="name_product" class=" form-control-label"><?= $this->l("Quantity") ?></label>
                    </div>
                    <div class="col col-md-4">
                        <input type="text" id="price" name="quantity" placeholder="quantity" class="form-control" value="<?=isset ($product['Quantity']) ? $product['Quantity']:''  ?>">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="Description" class=" form-control-label">Description</label>
                    </div>
                    <div class="col-12 col-md-9" >
                        <textarea name="Description" id="Description" rows="9" placeholder="Description..." class="ckeditor"><?= isset ($product['description']) ? $product['description']:'' ?></textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-9">
                            <input  class="btn btn-primary btn-sm" type="submit" value="Update">
                    </div>
                    <a class="col-12 col-md-3" href="<?= BASE_URL().'Admin/Products?Del='.$product['id_product'] ?>">
                    <input  class="btn btn-danger btn-sm" type="Button" value="Delete">
                    </a>
                </div>
            </form>
        </div>
        <?php } else { ?>
            <div class="card-body card-block">
            <form id="editProduct" method="post" enctype="multipart/form-data"  class="form-horizontal" >
                <div class="col-12 col-md-9">
                        <input type="file" id="file-input" name="file-input" class="form-control-file">
                        <img class="thumb" id="img_prview" />
                </div>
                <br>
                <input type="text" hidden id="id_product" name="id_product"  value="new">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="name_product" class=" form-control-label">Name</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="name_product" name="name_product" placeholder="name product" class="form-control" value="">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="Catalogs" class=" form-control-label">Categories</label>
                    </div>
                    <div class="col col-md-3">
                        
                        <?php foreach($main_cate as $m){ ?>
                            <input type="button" onclick="Select_cate(<?= $m['id_cate'] ?>)" class="btn btn-primary btn-sm"  value="<?= $m['name'] ?>">
                        <?php } ?>
                       
                    </div>
                    <div class="col col-md-6">
                        <select name="Catalogs[]" id="Catalogs" multiple="" class="form-control">
                            <?php foreach($sub_cate as $b){ ?>
                                <option id="subcate<?= $b['id_main_cate'] ?>" value="<?= $b['id_sub_cate'] ?>"><?= $b['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <div class="row form-group">
                    <div class="col col-md-2">
                        <label for="name_product" class=" form-control-label"><?= $this->l("Price") ?></label>
                    </div>
                    <div class="col col-md-4">
                        <input type="text" id="price" name="price" placeholder="price" class="form-control" >
                    </div>
                    <div class="col col-md-2">
                        <label for="name_product" class=" form-control-label"><?= $this->l("Sale Price") ?></label>
                    </div>
                    <div class="col col-md-4">
                        <input type="text" id="sell_price" name="sell_price" placeholder="<?= $this->l("Sale Price") ?>" class="form-control" >
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-2">
                        <label for="name_product" class=" form-control-label"><?= $this->l("Quantity") ?></label>
                    </div>
                    <div class="col col-md-4">
                        <input type="text" id="price" name="quantity" placeholder="quantity" class="form-control" value="<?=isset ($product['Quantity']) ? $product['Quantity']:''  ?>">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="Description" class=" form-control-label">Description</label>
                    </div>
                    <div class="col-12 col-md-9" >
                        <textarea name="Description" id="Description" rows="9" placeholder="Description..." class="ckeditor"></textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-9">
                            <input  class="btn btn-primary btn-sm" type="submit" value="Upload">
                    </div>
                    <a class="col-12 col-md-3" href="<?= BASE_URL().'Admin/Products' ?>">
                    <input  class="btn btn-danger btn-sm" type="Button" value="Cancel">
                    </a>
                </div>
            </form>
        </div>
        <?php } ?>
    </div>
</div>
<script>
function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

    // Only process image files.
    if (!f.type.match('image.*')) {
        continue;
    }

    var reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = (function(theFile) {
        return function(e) {
        // Render thumbnail.
        document.getElementById('img_prview').setAttribute("src", e.target.result);
        };
    })(f);

    // Read in the image file as a data URL.
    reader.readAsDataURL(f);
    }
}

document.getElementById('file-input').addEventListener('change', handleFileSelect, false);

function Select_cate(id = ''){
    $('[id^=subcate]').attr("hidden",true);
    var id_sub_cate = "#subcate"+id;
    $(id_sub_cate).attr("hidden",false);
}

</script>