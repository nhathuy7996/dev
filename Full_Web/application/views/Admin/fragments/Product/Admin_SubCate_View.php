<div class="main-content">
    <div class="card"> 
        <br>
        <div class="col-12 row form-group">
            <?php foreach($main_cate as $b){ ?>
                <div class="col-12 col-md-<?= 12/count($main_cate) ?>">
                    <a href="<?= BASE_URL().'Admin/SubCate?main='.$b['id_cate'] ?>">
                        <?php if(isset($current_main) && $current_main == $b['id_cate']) {?>
                            <input  class="btn btn-primary" type="button" value="<?= $b['name'] ?>">
                        <?php } else { ?>
                            <input  class="btn btn-outline-primary" type="button" value="<?= $b['name'] ?>">
                        <?php } ?>
                    </a>
                </div>
            <?php } ?>
        </div>     

        <br>
        <form method = "post" class="form-horizontal">
            <div class="col-12 row form-group">
                <div class="col col-md-3">
                    <label for="new_cate" class=" form-control-label">New Sub Cate</label>
                </div>
                <input hidden type="text" id="main_cate" name  ="main_cate" value = "<?= !empty($current_main)?$current_main : 1 ?>">
                <div class="col-12 col-md-6">
                    <input type="text" id="new_sub_cate" name="new_sub_cate" placeholder="name sub cate" class="form-control" >
                </div>
                <div class="col-12 col-md-3">
                    <input  class="btn btn-primary btn-sm" type="submit" value="Add">
                </div>
            </div>      
            <form>
        <br>
        <?php foreach($sub_cate as $s){ ?>
            <div class="col-12 row form-group">
                <div class="col-12 col-md-6">
                    <input type="text" id="cate_id_<?= $s['id_sub_cate'] ?>" name="name_cate" value="<?= $s['name'] ?>" class="form-control" >
                </div>
                <div class="col-12 col-md-3">
                    <button onclick="Edit(<?= $s['id_sub_cate'] ?>)"  class="btn btn-outline-primary" > Edit</button>
                </div>
                <a  class="col-12 col-md-3" href="<?= BASE_URL().'Admin/SubCate?Del='.$s['id_sub_cate'] ?>">             
                    <button type="button" class="btn btn-danger btn-sm">Delete</button>
                </a>
            </div>    
        <?php } ?>
    </div>
</div>
<script>
function Edit(id){
    var name = "#cate_id_" + id;
    var value = $(name).val();

    $.post("<?= BASE_URL() ?>Admin/SubCate",
    {
        cate_id: id,
        name_cate: value
    },
    function(data, status){
        alert("Edited!");
    });
}
</script>