<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= BASE_URL().'public/'?>images/<?= $setting['icon'] ?>">

    <title><?= $post['title'] ?></title>

    <?php $this->load->view('fragments/Header.load.php') ?>

    <!-- flexSlider CSS file -->
	<link rel="stylesheet" href="<?= BASE_URL().'public/' ?>vendor/woocommerce-FlexSlider/flexslider.css" type="text/css" media="screen" />
	
  </head>

  <body id="home">
  	
	<!-- loader start -->

	<div class="loader">
		<div id="awsload-pageloading">
			<div class="awsload-wrap">
				<ul class="awsload-divi">
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- loader end -->
    
	<?php $this->load->view('fragments/Topbar.fragment.php') ?>

    
    <!--Page Title-->    
	
	<div class="page_title_ctn"> 
		<div class="container">
			<div class="row">                        
				<div class="col-sm-12">
					<div class="page-title clearfix">
						
						<ol class="breadcrumb">
						  <li><a href="<?= BASE_URL() ?>">Home</a></li>
						  <li><a href="<?= BASE_URL().'Blog' ?>">Blog</a></li>
						</ol>
					</div>
				</div>
    		</div>           
    	</div>
    </div>
    
    <!-- Blog Post Style 1 -->
    
    <section class="blog-single" id="blog_s_post"><!-- Section id-->
        <div class="container">

            <div class="row">
                <div class="col-md-9 col-sm-8">
                    <div class="blog-posts single-post">

                        <article class="post post-large blog-single-post">
                            <div class="post-image">
                             <img class="img-responsive" src="images/blog-post/blog-single/blog-b-1.jpg" alt="">                                
                            </div>

                            <div class="post-date">           
                                <span class="day"><?= $post['date_create'] ?></span>
                            </div>

                            <div class="post-content">

                                <h2><a href="blog-post.html"><?= $post['title']?></a></h2>

                                <div class="post-meta">
                                    <span><i class="fa fa-user"></i> By <a href="#"><?= $post['author']?></a> </span>
                                    <span><i class="fa fa-tag"></i> <a href="#">Duis</a>, <a href="#">News</a> </span>
                                    <span><i class="fa fa-comments"></i> <a href="#">12 Comments</a></span>
                                </div>
                                
                                <?= $post['content'] ?>

                                <div class="post-block post-share">
                                    <h3><i class="fa fa-share"></i>Share this post</h3>

                                    <!-- AddThis Button BEGIN -->
                                    <div class="addthis_toolbox addthis_default_style ">
                                        <a class="addthis_button_facebook_like"></a>
                                        <a class="addthis_button_tweet"></a>
                                        <a class="addthis_button_pinterest_pinit"></a>
                                        <a class="addthis_counter addthis_pill_style"></a>
                                    </div>
                                    <!--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>-->
                                    <!-- AddThis Button END -->

                                </div>

                                <div class="post-block post-author clearfix">
                                    <h3><i class="fa fa-user"></i>Author</h3>
                                    <div class="img-thumbnail">
                                        <a href="blog-post.html">
                                            <img src="images/blog-post/blog-avtar/avatar-1.jpg" alt="">
                                        </a>
                                    </div>
                                    <p><strong class="name"><a href="#"><?=$post['author']?></a></strong></p>
                                   
                                </div>
                                <div class="post-block post-comments clearfix">
                                    <h3><i class="fa fa-comments"></i><?= $this->l('Comments') ?> (<?= count($comment) ?>)</h3>

                                    <ul class="comments">
                                        <?php foreach($comment as $cmt){ 
                                            $id = $cmt['user_id'];
                                            $sql = "SELECT username,avatar  FROM users WHERE id_user = $id";
                                            $query = $this->db->query($sql);
                                            $user = $query->result_array()[0];  ?>
                                        <li>
                                            <div class="comment">
                                                <div class="img-thumbnail">
                                                    <img class="avatar" alt="" src="<?= BASE_URL() ?>public/images/avatars/<?= $user['avatar'] ?>">
                                                </div>
                                                <div class="comment-block">
                                                    <div class="comment-arrow"></div>
                                                    <span class="comment-by">
                                                        <strong><?= $user['username'] ?></strong>
                                                    </span>
                                                    <p><?= $cmt['content'] ?></p>
                                                    <span class="date pull-right"><?= $cmt['date_create'] ?></span>
                                                </div>
                                            </div>

                                            <!--
                                            <ul class="comments reply">
                                                <li>
                                                    <div class="comment">
                                                        <div class="img-thumbnail">
                                                            <img class="avatar" alt="" src="images/blog-post/blog-avtar/avatar-1.jpg">
                                                        </div>
                                                        <div class="comment-block">
                                                            <div class="comment-arrow"></div>
                                                            <span class="comment-by">
                                                                <strong>John Doe</strong>
                                                                <span class="pull-right">
                                                                    <span> <a href="#"><i class="fa fa-reply"></i> Reply</a></span>
                                                                </span>
                                                            </span>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae, gravida pellentesque urna varius vitae.</p>
                                                            <span class="date pull-right">January 12, 2013 at 1:38 pm</span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="comment">
                                                        <div class="img-thumbnail">
                                                            <img class="avatar" alt="" src="images/blog-post/blog-avtar/avatar-2.jpg">
                                                        </div>
                                                        <div class="comment-block">
                                                            <div class="comment-arrow"></div>
                                                            <span class="comment-by">
                                                                <strong>John Doe</strong>
                                                                <span class="pull-right">
                                                                    <span> <a href="#"><i class="fa fa-reply"></i> Reply</a></span>
                                                                </span>
                                                            </span>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae, gravida pellentesque urna varius vitae.</p>
                                                            <span class="date pull-right">January 12, 2013 at 1:38 pm</span>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            -->
                                        </li>
                                        <?php } ?>
                                        <li>
                                            <div class="comment">   
                                                <form method="POST"> 
                                                    <input class="form-control" id="comment" name="comment" type="text">
                                                    <input class="btn btn-primary btn-lg" type="submit" value="<?= $this->l("Comment")?>">
                                                </form>
                                            </div>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </article>

                    </div>
                </div>
				
                
                <div class="col-md-3 col-sm-4">
                    <aside class="sidebar">

                        <hr class="sep-line" />

                        <h4 class="blue">Categories</h4>
                        <ul class="nav  primary">
                            <?php  foreach($themes as $t){ ?>
                                <li><a href="<?= BASE_URL().'Blog/'.$t['id'] ?>"><?= $t['name'] ?></a></li>
                            <?php } ?>  
                        </ul>

                        <!--
                        <div class="tabs">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#popularPosts" data-toggle="tab"><i class="fa fa-star"></i> Popular</a></li>
                                <li><a href="#recentPosts" data-toggle="tab">Recent</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="popularPosts">
                                    <ul class="simple-post-list">
                                        <li>
                                            <div class="post-image">
                                                <div class="img-thumbnail">
                                                    <a href="#">
                                                        <img src="images/blog-post/blog-avtar/avatar-1-50x50.jpg" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="post-info">
                                                <a href="#">Nullam Vitae Nibh Un Odiosters</a>
                                                <div class="post-meta">
                                                     Jan 10, 2017
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="post-image">
                                                <div class="img-thumbnail">
                                                    <a href="#">
                                                        <img src="images/blog-post/blog-avtar/avatar-2-50x50.jpg" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="post-info">
                                                <a href="#">Vitae Nibh Un Odiosters</a>
                                                <div class="post-meta">
                                                     Jan 10, 2017
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="post-image">
                                                <div class="img-thumbnail">
                                                    <a href="#">
                                                        <img src="images/blog-post/blog-avtar/avatar-1-50x50.jpg" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="post-info">
                                                <a href="#">Odiosters Nullam Vitae</a>
                                                <div class="post-meta">
                                                     Jan 10, 2017
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-pane" id="recentPosts">
                                    <ul class="simple-post-list">
                                        <li>
                                            <div class="post-image">
                                                <div class="img-thumbnail">
                                                    <a href="#">
                                                        <img src="images/blog-post/blog-avtar/avatar-2-50x50.jpg" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="post-info">
                                                <a href="#">Vitae Nibh Un Odiosters</a>
                                                <div class="post-meta">
                                                     Jan 10, 2017
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="post-image">
                                                <div class="img-thumbnail">
                                                    <a href="#">
                                                        <img src="images/blog-post/blog-avtar/avatar-1-50x50.jpg" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="post-info">
                                                <a href="#">Odiosters Nullam Vitae</a>
                                                <div class="post-meta">
                                                     Jan 10, 2017
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="post-image">
                                                <div class="img-thumbnail">
                                                    <a href="#">
                                                        <img src="images/blog-post/blog-avtar/avatar-2-50x50.jpg" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="post-info">
                                                <a href="#">Nullam Vitae Nibh Un Odiosters</a>
                                                <div class="post-meta">
                                                     Jan 10, 2017
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <hr class="sep-line" />

                        <h4 class="blue">About Us</h4>
                        <p>Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Nulla nunc dui, tristique in semper vel. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. </p>

                    </aside>
                    <aside class="widget widget_testimonial">
                    	<hr class="sep-line" />
						<h4 class="blue">Testimonials</h4>
                            <div class="widgetClientTestimonial">
                              <ul class="slides">
                                <li data-thumb="images/blog-post/blog-avtar/thumb-1.jpg">
                                      <blockquote>
                                        <p>Lorem Ipsum is simply dummy text of the printing &amp; pesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since they 1500s, when an unknown printer took.</p>
                                        <footer class="client-name">GFXPARTNER <span class="client-title"> - CEO</span></footer>
                                      </blockquote>
                                </li>

                                <li data-thumb="images/blog-post/blog-avtar/thumb-2.jpg">
                                      <blockquote>
                                        <p>Lorem Ipsum is simply dummy text of the printing &amp; pesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since they 1500s, when an unknown printer took.</p>
                                        <footer class="client-name">GFXPARTNER <span class="client-title">CEO</span></footer>
                                      </blockquote>
                                </li>

                                <li data-thumb="images/blog-post/blog-avtar/thumb-3.jpg">
                                      <blockquote>
                                        <p>Lorem Ipsum is simply dummy text of the printing &amp; pesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since they 1500s, when an unknown printer took.</p>
                                        <footer class="client-name">GFXPARTNER <span class="client-title">CEO</span></footer>
                                      </blockquote>
                                </li>

                                <li data-thumb="images/blog-post/blog-avtar/thumb-4.jpg">
                                      <blockquote>
                                        <p>Lorem Ipsum is simply dummy text of the printing &amp; pesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since they 1500s, when an unknown printer took.</p>
                                        <footer class="client-name">GFXPARTNER <span class="client-title">CEO</span></footer>
                                      </blockquote>
                                </li>

                                <li data-thumb="images/blog-post/blog-avtar/thumb-5.jpg">
                                      <blockquote>
                                        <p>Lorem Ipsum is simply dummy text of the printing &amp; pesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since they 1500s, when an unknown printer took.</p>
                                        <footer class="client-name">GFXPARTNER <span class="client-title">CEO</span></footer>
                                      </blockquote>
                                </li>

                                <li data-thumb="images/blog-post/blog-avtar/thumb-6.jpg">
                                      <blockquote>
                                        <p>Lorem Ipsum is simply dummy text of the printing &amp; pesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since they 1500s, when an unknown printer took.</p>
                                        <footer class="client-name">GFXPARTNER <span class="client-title">CEO</span></footer>
                                      </blockquote>
                              	</li>
							</ul>
						</div>
					</aside>
                    <aside class="widget widget_tag_cloud">
                    	<hr class="sep-line" />
						<h4 class="blue">Popular Tags</h4>
                        <ul class="list-inline clearfix">
                            <li><a href="#">Amazing</a></li>
                            <li><a href="#">Envato</a></li>
                            <li><a href="#">Themes</a></li>
                            <li><a href="#">Clean</a></li>
                            <li><a href="#">Responsiveness</a></li>
                            <li><a href="#">SEO</a></li>
                            <li><a href="#">Mobile</a></li>
                            <li><a href="#">IOS</a></li>
                            <li><a href="#">Flat</a></li>
                            <li><a href="#">Design</a></li>
                        </ul>
                    </aside>
                    -->
                </div>                
            </div>
		</div>
    </section>           
                   
    <?php $this->load->view('fragments/Footer.fragment.php') ?>

    <!-- FlexSlider -->
    <script defer src="<?= BASE_URL().'public/' ?>vendor/woocommerce-FlexSlider/jquery.flexslider.js"></script> 


  </body>
</html>
