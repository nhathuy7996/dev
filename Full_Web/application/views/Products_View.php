<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= BASE_URL().'public/'?>images/<?= $setting['icon'] ?>">

    <title>Wine Store - Html Responsive Template</title>

    <?php $this->load->view('fragments/Header.load.php'); ?>

     
  </head>

  <body id="home">
  	
	<!-- loader start -->

	<div class="loader">
		<div id="awsload-pageloading">
			<div class="awsload-wrap">
				<ul class="awsload-divi">
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- loader end -->
    
    <?php $this->load->view('fragments/Topbar.fragment.php'); ?>

    
    <!--Page Title-->
    
	<div class="page_title_ctn"> 
		<div class="container">
			<div class="row">                        
				<div class="col-sm-12">
					<div class="page-title clearfix">
						<h3>Shop</h3>
						<ol class="breadcrumb">
						  <li><a href="<?= base_url() ?>">Home</a></li>
						  <li class="active"><span>Shop</span></li>
						</ol>
					</div>
				</div>
    		</div>           
    	</div>
    </div>  
    
    <!--Shoping with Sidebar Section-->
    
		<section class="sidebar-shop shop-pages dart-pt-20">
            <div class="container">
                <div class="content-wrap ">
                    <div class="shorter">
                        <!--
                        <div class="row">                        
                            <div class="col-sm-6 col-xs-6">
                                <span class="showing-result">Showing 1–12 of 30 products</span>
                            </div>
                            <div class="col-sm-6 col-xs-6 text-right">
                                <div class="short-by">                                     
                                    <span class="dart-pr-10">short by</span>
                                    <select  class="selectpicker form-control" >
                                        <option>Newnest</option>
                                        <option>Type 1</option>
                                        <option>Type 2</option>
                                        <option>Type 3</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        -->
                    </div>
                    <div class="row"> 
                        <div class="col-sm-3 col-md-4 col-lg-3">
                            <div class="shop-sidebar mt-20">
                                <aside class="widget">
                                    <h2 class="widget-title">Categories</h2>
                                    <div class="panel-group shop-links-widget" id="accordion" role="tablist" aria-multiselectable="true">
                                        <?php foreach($main_cate as $m) { ?>
                                            <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="heading<?= $m['id_cate'] ?>">
                                                        <h4 class="panel-title">
                                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $m['id_cate'] ?>" aria-expanded="false" aria-controls="collapse<?= $m['id_cate'] ?>"><?= $m['name'] ?><span class="caret"></span></a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse<?= $m['id_cate'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?= $m['id_cate'] ?>">
                                                        <div class="panel-body">
                                                            <ul class="list-unstyled">
                                                                <?php foreach($sub_cate as $s){ 
                                                                    if($s['id_main_cate'] == $m['id_cate']) {?>
                                                                        <li><a href="<?= BASE_URL().'Products/'.$s['id_sub_cate'] ?>">
                                                                            <?= $s['name'] ?></a></li>
                                                                    <?php } } ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php } ?>
                                    </div>  
                                </aside>
                                <!--
                                <aside class="widget">
                                    <h2 class="widget-title">Colors</h2>
                                    <ul class="list-unstyled">
                                        <li> <a href="#">Black</a> </li>
                                        <li> <a href="#">Blue</a> </li>
                                        <li> <a href="#">Yellow</a> </li>
                                        <li> <a href="#">Grey</a> </li>
                                        <li> <a href="#">Anthracite</a> </li>
                                        <li> <a href="#">White</a> </li>                                                    
                                    </ul>
                                </aside>
                                <aside class="widget widget_size">
                                    <h2 class="widget-title">Categories</h2>
                                    <ul class="list-unstyled">
                                        <li> <a href="#">p</a> </li>
                                        <li> <a href="#">s</a> </li>
                                        <li> <a href="#">m</a> </li>
                                        <li> <a href="#">l</a> </li>
                                        <li> <a href="#">xl</a> </li>                                                                                                      
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li> <a href="#">2</a> </li>
                                        <li> <a href="#">2</a> </li>
                                        <li> <a href="#">6</a> </li>
                                        <li> <a href="#">8</a> </li>
                                        <li> <a href="#">10</a> </li>                                                                                                      
                                    </ul>
                                </aside>
                                <aside class="widget widget_size">
                                    <h2 class="widget-title">Price</h2>
                                    <div class="widget-content">
                                        <div id="slider-range" class="slider-range"></div>
                                        <label  for="amount">Price</label> <input type="text" id="amount" readonly />  
                                        <span><a class="btn filter-btn btn-default" href="#">Filter</a></span>
                                    </div> 
                                </aside>-->
                            </div>
                        </div>

                        <div class="col-sm-9 col-md-8 col-lg-9 border-lft">
                            <div class="product-wrap">
                                <div class="row">   
									<?php foreach($Products as $p) {?>
										<div class="col-sm-4 col-md-4">
											<div class="wa-theme-design-block">
												<figure class="dark-theme">
													<img src="<?= BASE_URL().'public/' ?>images/product/details/<?= $p['id_product'] ?>/<?= isset(json_decode( $p['image'])[0])? json_decode( $p['image'])[0] : '' ?>" alt="Product <?= $p['name'] ?>">

                                                    <span class="block-sticker-tag1">
                                                    <span onclick="AddToCart(<?= $p['id_product'] ?>)" class="off_tag"><strong><i class="fa fa-shopping-basket" aria-hidden="true"></i></strong></span>
                                                    </span>	
													<span class="block-sticker-tag2">
													<span class="off_tag2 btn-action btn-quickview" data-toggle="modal" data-target="#quickView">
														<strong><a href="<?= BASE_URL().'Products/Detail/'.$p['id_product'] ?>"><i class="fa fa-eye" aria-hidden="true"></i></a></strong>
													</span>

													</span>
												</figure>
										<div class="block-caption1">
											<h4><?= $p['name'] ?></h4>
											<div class="clear"></div>
											<div class="price">
												<span class="sell-price"><?= $p['sell_price'] ?>VNĐ</span>
												<?php if($p['price'] != $p['sell_price']) { ?>
													<span class="actual-price"><?= $p['price'] ?>VNĐ</span>
												<?php } ?>
											</div>
										</div>
											</div>
										</div>
									<?php } ?>
                                </div>
                            </div>
                            <!-- Phân trang -->
                            <div class="pagination-wrap text-right">
                                <ul>
                                    <li class="disabled"><a href="<?=  BASE_URL().'Products/'.$catalog?>Page/<?php if($page > 1) echo $page-1; else echo $page; ?>">
										<span class="fa fa-long-arrow-left"></span></a></li>

									<?php for($i = 0; $i<$pages_number; $i++){ 
											if($i+1 == $page) {	?>
												<li class="active"><a href="<?= BASE_URL().'Products/'.$catalog?>Page/<?= $i+1 ?>">
													<?= $i+1 ?><span class="sr-only">(current)</span></a></li>
											<?php } else { ?>
												<li><a href="<?= BASE_URL().'Products/'.$catalog?>Page/<?= $i+1 ?>">
													<?= $i+1 ?></a></li>						
									<?php } }   ?>								
                                    <li class="next"><a href="<?=  BASE_URL().'Products/'.$catalog?>Page/<?php if($page < $pages_number) echo $page+1; else echo $page; ?>">
										 <span class="fa fa-long-arrow-right"></span> </a> </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>    
    
        <script>
		function AddToCart(id){
			var link = '<?= BASE_URL() ?>/Products/Detail/' + id;
			$.post(link,
			{
				number_product: 1
			}, function(data, status){
						alert("<?= $this->l('Add to cart!') ?>");
					});
			
		}
	</script>
                   
  <?php $this->load->view('fragments/Footer.fragment.php') ?>
	  

  </body>
</html>
