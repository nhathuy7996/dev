<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= BASE_URL().'public/'?>images/<?= $setting['icon'] ?>">

    <title><?= $this->l('Blog') ?></title>

    <?php $this->load->view('fragments/Header.load.php')?>

     
  </head>

  <body id="home">
  	
	<!-- loader start -->

	<div class="loader">
		<div id="awsload-pageloading">
			<div class="awsload-wrap">
				<ul class="awsload-divi">
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- loader end -->
    
	<?php $this->load->view('fragments/Topbar.fragment.php') ?>

    
    <!--Page Title-->
    
	<div class="page_title_ctn"> 
		<div class="container">
			<div class="row">                        
				<div class="col-sm-12">
					<div class="page-title clearfix">
						<h3>Blog</h3>
						<ol class="breadcrumb">
						  <li><a href="<?= BASE_URL() ?>">Home</a></li>
                          <li><a href="<?= BASE_URL().'Blog/' ?>">Blog</a></li>

                          <?php if(isset($theme['name'])) { ?>
                          <li class="active"><span><?= $theme['name'] ?></span></li>
                          <?php }?>
                          
						</ol>
					</div>
				</div>
    		</div>           
    	</div>
    </div>
    
    <!-- Blog Post Style 1 -->    
    <section class="blogstyle-1">
        <div class="container">
            <div class="row">
                <?php foreach($post as $p) {?>
                    <div class="col-md-4 col-sm-4">
                        <article class="blog-post-container clearfix">
                            <div class="post-thumbnail">
                                <img src="<?= BASE_URL().'public/' ?>images/blog-post/<?= $p['picture'] ?>" class="img-responsive " alt="Image">
                            </div><!-- /.post-thumbnail -->
                            
                            <div class="blog-content">
                                <div class="dart-header">
                                    <h4 class="dart-title"><a href="blog-single.html"><?= $p['title']?></a></h4>
                                    <div class="dart-meta">
                                        <ul class="list-unstyled">
                                            <li><span class="author"> By: <a href="#"><?= $p['author'] ?></a></span></li>
                                            <li><span class="posted-date"><a href="#"><?= $p['date_create'] ?></a></span></li>
                                        </ul>
                                    </div><!-- /.dart-meta -->
                                </div><!-- /.dart-header -->
        
                                <div class="dart-content">
                                    <p><?= $p['summary'] ?> ...</p>
                                </div><!-- /.dart-content -->
        
                                <div class="dart-footer">
                                    <ul class="dart-meta clearfix list-unstyled">
                                        
                                        <li><a class="pull-right" href="<?= BASE_URL().'Blog/Detail/'.$p['id_post'] ?>"> More <i class="fa fa-angle-double-right"></i></a></li>
                                    </ul>
                                </div><!-- /.dart-footer -->
                            </div><!-- /.blog-content -->
                        </article>
                    </div>
                <?php } ?>
            </div><!-- /.row -->
                            <!-- Phân trang-->
                            <div class="pagination-wrap text-right">
                                <ul>
                                    <li class="disabled"><a href="<?=  BASE_URL().'Blog/' ?><?= !empty($theme['id']) ? $theme['id'].'/' : '' ?>Page/<?php if($page > 1) echo $page-1; else echo $page; ?>">
										<span class="fa fa-long-arrow-left"></span></a></li>

									<?php for($i = 0; $i<$pages_number; $i++){ 
											if($i+1 == $page) {	?>
												<li class="active"><a href="<?=  BASE_URL().'Blog/' ?><?= !empty($theme['id']) ? $theme['id'].'/' : '' ?>Page/<?= $i+1 ?>">
													<?= $i+1 ?><span class="sr-only">(current)</span></a></li>
											<?php } else { ?>
												<li><a href="<?=  BASE_URL().'Blog/' ?><?= !empty($theme['id']) ? $theme['id'].'/' : '' ?>Page/<?= $i+1 ?>">
													<?= $i+1 ?></a></li>						
									<?php } }   ?>								
                                    <li class="next"><a href="<?=  BASE_URL().'Blog/' ?><?= !empty($theme['id']) ? $theme['id'].'/' : '' ?>Page/<?php if($page < $pages_number) echo $page+1; else echo $page; ?>">
										 <span class="fa fa-long-arrow-right"></span> </a> </li>
                                </ul>
                            </div>
                <!-- Phân trang-->
        </div> 
    </section>            
                   
    <?php $this->load->view('fragments/Footer.fragment.php') ?>

  </body>
</html>
