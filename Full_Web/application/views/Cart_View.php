<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= BASE_URL().'public/' ?>images/<?= $setting['icon'] ?>">

    <title><?= $this->l("Cart") ?></title>

    <?php $this->load->view('fragments/Header.load.php') ?>

     
  </head>

  <body id="home">
  	
	<!-- loader start -->

	<div class="loader">
		<div id="awsload-pageloading">
			<div class="awsload-wrap">
				<ul class="awsload-divi">
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- loader end -->
    
    <?php $this->load->view('fragments/Topbar.fragment.php') ?>
    
    <!--Page Title-->
    
	<div class="page_title_ctn"> 
		<div class="container">
			<div class="row">                        
				<div class="col-sm-12">
					<div class="page-title clearfix">
						<h3>Checkout</h3>
						<ol class="breadcrumb">
						  <li><a href="<?= BASE_URL() ?>">Home</a></li>
						  <li class="active"><span>Checkout</span></li>
						</ol>
					</div>
				</div>
    		</div>           
    	</div>
    </div>
    
    <!--Shoping Cart-->
    <section class="dart-pt-30">
    	<div class="container">
        	
         
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h3 class="">Shipping Address</h3>

                    <form id="shipping-form" name="shipping-form" class="row dart-pt-20"  method="post">

                        <div class="col-sm-6">
                            <label for="shipping-form-name">Name:</label>
                            <input type="text" id="shipping-form-name" name="name" value="" class="dart-form-control" />
                        </div>

                        <div class="col-sm-6 col_last">
                            <label for="shipping-form-lname">Last Name:</label>
                            <input type="text" id="shipping-form-lname" name="lastname" value="" class="dart-form-control" />
                        </div>

                        <div class="clear"></div>

                        <div class="col-sm-12">
                            <label for="shipping-form-companyname">Phone Number:</label>
                            <input type="text" id="shipping-form-companyname" name="compname" value="" class="dart-form-control" />
                        </div>

                        <div class="col-sm-12">
                            <label for="shipping-form-address">Address:</label>
                            <input type="text" id="shipping-form-address" name="address1" value="" class="dart-form-control" />
                        </div>

                        <div class="col-sm-12">
                            <input type="text" id="shipping-form-address2" name="address2" value="" class="dart-form-control" />
                        </div>

                        <div class="col-sm-12">
                            <label for="shipping-form-city">City / Town</label>
                            <input type="text" id="shipping-form-city" name="city" value="" class="dart-form-control" />
                        </div>

                        <div class="col-sm-12">
                            <label for="shipping-form-message">Notes <small>*</small></label>
                            <textarea class="dart-form-control" id="shipping-form-message" name="note" rows="6" cols="30"></textarea>
                        </div>

                        <?php if(isset($cart) && !empty($cart)) foreach($cart as $c){
                                unset($c->description);
                        } ?>

                        <textarea hidden name="cart" form="shipping-form"><?= json_encode($cart) ?></textarea>
                       
                    </form>
                </div>
				
                <div class="col-md-6 col-sm-6">
   				<div class="table-responsive">
    				<h3 class="dart-pb-20">Your Orders</h3>
                    <table class="table cart checkout">
                        <thead>
                            <tr>
                                <th class="cart-product-thumbnail">Product</th>
                                <th class="cart-product-name">Description</th>
                                <th class="cart-product-quantity">Quantity</th>
                                <th class="cart-product-subtotal">Total</th>
                                <th class="cart-product-subtotal">Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0; if(isset($cart) && !empty($cart)) foreach($cart as $c){ ?>
                            <tr class="cart_item">
    
                                <td class="cart-product-thumbnail">
                                    <a href="<?= BASE_URL().'Products/Detail/'.$c->id_product ?>">
                                        <img width="64" height="64" src="<?= BASE_URL().'public/' ?>images/product/details/<?= $c->id_product ?>/<?= json_decode($c->image)[0] ?>"
                                         alt="<?= $c->name ?>"></a>
                                </td>
    
                                <td class="cart-product-name">
                                    <a href="<?= BASE_URL().'Products/Detail/'.$c->id_product ?>">
                                    <?= $c->name ?>
                                    </a>
                                </td>
    
                                <td class="quantity">
                                    <span><?= $c->quantity ?></span>
                                </td>
    
                                <td class="cart-product-subtotal">
                                    <span class="amount"><?= $c->quantity * $c->sell_price ?> VNĐ</span>
                                </td>
        
                                <td class="cart-product-remove">
                                    <a href="<?= BASE_URL()?>Cart?id=<?= $c->id_product ?>" class="remove" title="Remove this item"><i class="fa fa-remove"></i></a>
                                </td>
      
                            </tr>
                            <?php $total += $c->quantity * $c->sell_price; } ?>
                        </tbody>
                    </table>
                </div>

                
    		</div>
            
            <div class="col-md-6  col-sm-6 clearfix">
                    <div class="table-responsive totle-cart">
                        <h3 class="dart-pb-20">Cart Totals</h3>

                        <table class="table cart">
                            <tbody>
                                <tr class="cart_item cart_totle">
                                    <td class="cart-product-name">
                                        <strong>Cart Subtotal</strong>
                                    </td>

                                    <td class="cart-product-name">
                                        <span class="amount"><?= $total ?> VNĐ</span>
                                    </td>
                                </tr>
                                <tr class="cart_item cart_totle">
                                    <td class="cart-product-name">
                                        <strong>Shipping</strong>
                                    </td>

                                    <td class="cart-product-name">
                                        <span class="amount"><?= $ship ?> VNĐ</span>
                                    </td>
                                </tr>
                                <tr class="cart_item cart_totle">
                                    <td class="cart-product-name">
                                        <strong>Total</strong>
                                    </td>

                                    <td class="cart-product-name">
                                        <span class="blue"><strong><?= $total ?> VNĐ</strong></span>
                                    </td>
                                </tr>
                            </tbody>

                        </table>

                    </div>
                    
                    <div class="payments-options">
                    <ul class="list-unstyled">
                        <li>
                            <label id="direct-transfer" for="payment_method_bacs">
                                <input id="payment_method_bacs" class="input-radio" type="radio" data-order_button_text="" value="bacs" name="payment_method">
                                Cash On Delivery
                            </label>
                        </li>
 
                    </ul>
                </div>
                    
                </div>
            	
                <div class="col-md-12">
                	<input type="submit" form="shipping-form" class="btn rd-3d-btn dart-btn-sm dart-fright" value="Place Order" />
                </div>
                
            </div>
        </div>    	
    </section>            
                   
    <?php $this->load->view('fragments/Footer.fragment.php'); ?>

  </body>
</html>
