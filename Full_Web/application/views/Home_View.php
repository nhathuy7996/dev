<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= BASE_URL().'public/' ?>images/<?= $setting['icon']?>">

    <title><?= $setting['name_web'] ?></title>

    <?php $this->load->view('fragments/Header.load.php') ?>

   	<!-- Base MasterSlider style sheet -->
    <link rel="stylesheet" href="<?= BASE_URL().'public/' ?>vendor/masterslider/style/masterslider.css" />
     
    <!-- Master Slider Skin -->
    <link href="<?= BASE_URL().'public/' ?>vendor/masterslider/skins/default/style.css" rel="stylesheet" type="text/css" />
    
    <!-- masterSlider Template Style -->
	<link href="<?= BASE_URL().'public/' ?>vendor/masterslider/style/ms-layers-style.css" rel="stylesheet" type="text/css">   
   
    <!-- owl Slider Style -->
    <link rel="stylesheet" href="<?= BASE_URL().'public/' ?>vendor/owlcarousel/dist/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="<?= BASE_URL().'public/' ?>vendor/owlcarousel/dist/assets/owl.theme.default.min.css">

     
  </head>

  <body id="home">
  	
	<!-- loader start -->

	<div class="loader">
		<div id="awsload-pageloading">
			<div class="awsload-wrap">
				<ul class="awsload-divi">
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- loader end -->

   <?php $this->load->view('fragments/Topbar.fragment.php') ?>

    
    <!--Slider Here-->
    <section class="dart-no-padding-tb"> 
        
        <div class="ms-layers-template">
            <!-- masterslider -->
            <div class="master-slider ms-skin-black-2 round-skin" id="masterslider">
				<?php foreach($link_slide as $l) { ?>
                <div class="ms-slide slide-1" style="z-index: 10" data-delay="10">
                    <img src="<?= BASE_URL().'public/' ?>vendor/masterslider/style/blank.gif" data-src="<?= BASE_URL().'public/' ?>images/slide/<?= $l['link'] ?>" alt="<?= $l['alt'] ?>"/>
					<?= $l['setting'] ?>
				</div>                
                <?php } ?>                              
            </div>
            <!-- end of masterslider -->
        </div>
        <!-- end of template -->
    </section>   
    
    <div class="clearfix"></div>    
    
    <section class="dart-no-padding">
    	<div class="container-fluid">
    		<div class="row no-gutter">
				<?php foreach($link_promo as $l) { ?>
					<div class="col-lg-4 col-md-4 col-sm-4">
    					<p class="lineeffect"><a href="#"><img src="<?= BASE_URL().'public/' ?>images/promo/<?= $l['link'] ?>" class="img-responsive promo" alt="<?= $l['alt'] ?>"></a></p>
    				</div>
				<?php } ?>
    		</div>    		
    	</div>   	
    </section>

         
          
	<section class="super-deal-section"	style="background-image: url(<?= base_url() ?>public/images/<?= $setting['intro_image'] ?>);" >
    	<div class="container">
    		<div class="row">
    			<div class="col-md-6 col-sm-8 col-md-push-6 col-sm-push-4">
    				<div class="super-deal">
    					<?= $setting['intro'] ?>    <br>					
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
       
       
    <section class="product-slide">
        <div class="container">
           	<div class="row">
           		<div class="dart-headingstyle-one text-center">  <!--Style 1-->
					<h2 class="dart-heading"><?= $this->l('Featured Products') ?></h2>
		    		<img src="<?= BASE_URL().'public/' ?>images/Icon-sep.png" alt="img" />
			    </div>
           	</div>
            <div class="row">
                <div class="col-md-12">
                    <div role="tabpanel" class="tabSix text-center"><!--Style 6-->

                      <!-- Nav tabs -->
                      <ul id="tabSix" class="nav nav-tabs">
                        
                        <li>
                            <a href="#contentSix-two" data-toggle="tab">
                                <p><?= $this->l('New Arrival') ?></p>
                            </a>
                        </li>
  
                      </ul>

                      <!-- Tab panes -->
                        <div class="tab-content" id="contentSix-two">
                            <div class="owl-carousel owl-theme product-slider">
								<?php foreach($new as $p){ ?>
									<div class="wa-theme-design-block">
										<figure class="dark-theme">
											<img src="<?= BASE_URL().'public/' ?>images/product/details/<?= $p['id_product'] ?>/<?= isset(json_decode( $p['image'])[0])? json_decode( $p['image'])[0] : '' ?>" alt="Product">
											<?php if($p['price'] != $p['sell_price']){ ?>
												<div class="ribbon"><span>Sale</span></div>
											<?php } ?>
											<span class="block-sticker-tag1">
											<span onclick="AddToCart(<?= $p['id_product'] ?>)" class="off_tag"><strong><i class="fa fa-shopping-basket" aria-hidden="true"></i></strong></span>
											</span>	
											<span class="block-sticker-tag2">
											<span onclick="Detail(<?= $p['id_product'] ?>)" class="off_tag2 btn-action btn-quickview" data-toggle="modal" data-target="#quickView"><strong><i class="fa fa-eye" aria-hidden="true"></i></strong></span>
											</span>
										</figure>
										<div class="block-caption1">
											<h4><?= $p['name'] ?></h4>
											<div class="clear"></div>
											<div class="price">
												<span class="sell-price"><?= $p['price'] ?> VNĐ</span>
												<?php if($p['price'] != $p['sell_price']){ ?>
													<span class="actual-price"><?= $p['sell_price'] ?> VNĐ</span>
												<?php } ?>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
                        </div>

                      </div>
                    </div>              
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
	  
	
              
               
    <section class="blog">
    	<div class="container">
    		<div class="row">
    			<div class="dart-headingstyle-one dart-mb-60 text-center">  <!--Style 1-->
					<h2 class="dart-heading"><?= $this->l("Latest Blog") ?></h2>
		    		<img src="<?= BASE_URL().'public/' ?>images/Icon-sep.png" alt="img" />
			    </div>    			
    		</div>
    		
    		<div class="row no-gutter">
				<?php for($i = 0; $i< count($post); $i++){ $p = $post[$i];
						if($i % 2 == 0) {?>
					<div class="col-md-4 col-sm-4">
						<div class="blog-wapper">
							<div class="blog-img ImageWrapper">
								<a href="#" class="bubble-top">
									<img width="370" height="211" src="<?= BASE_URL().'public/' ?>images/blog-post/<?= $p['picture'] ?>" class="img-responsive" alt="blog"/>
									<div class="PStyleHe"></div>
								</a>
							</div>
							<div class="blog-content">
								<h4 class="blog-title"><a href="#"><?= $p['title'] ?></a></h4>
								<p class="post-date"><?= $p['date_create'] ?></p>
								<p class="post-content"><?= $p['summary'] ?></p>
								<a href="<?= BASE_URL().'Blog/Detail/'.$p['id_post'] ?>"><?= $this->l("Read More") ?></a>
							</div>
						</div>
					</div>
				<?php } else {?>
					<div class="col-md-4 col-sm-4">
						<div class="blog-wapper">
							<div class="blog-content">
								<h4 class="blog-title"><a href="#"><?= $p['title'] ?></a></h4>
								<p class="post-date"><?= $p['date_create'] ?></p>
								<p class="post-content"><?= $p['summary'] ?></p>
								<a href="<?= BASE_URL().'Blog/Detail/'.$p['id_post'] ?>"><?= $this->l("Read More") ?></a>
							</div>
							<div class="blog-img ImageWrapper">
								<a href="#" class="bubble-top">
									<img width="370" height="211" src="<?= BASE_URL().'public/' ?>images/blog-post/<?= $p['picture'] ?>" class="img-responsive" alt="blog"/>
									<div class="PStyleHe"></div>
								</a>
							</div>		
						</div>
					</div>
				<?php } } ?>
    		</div>    		
    	</div>
    </section>  
	  
	<section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
					<div class="owl-carousel owl-theme partnerlogo-slide">
						<?php foreach($partner_logo as $p){ ?>
						<div class="item">
							<img src="<?= BASE_URL().'public/' ?>images/partner/<?= $p['link'] ?>" alt="" />
						</div>
						<?php } ?>
					</div>
                                  
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
	
	<!--
    <section class="newsletter-bg">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-6 col-sm-6">
					<div class="dart-headingstyle-one dart-mb-20 text-left dart-pt-40 dart-pb-30">  
						<h2 class="dart-heading">Newsletter</h2>
						<p>Sign Up to our newsletter and Save 20% off on next purchase</p>
					</div> 
			    </div> 
    			<div class="col-md-6 col-sm-6">
    				<form class="form-inline">
						<div class="newsletter dart-pt-70 dart-pb-30">
						  <div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
						  </div>
						  <button type="submit" class="btn btn-default">Subcribe<i class="fa fa-envelope-o"></i></button>
						</div>
					</form>
    			</div>
    		</div>
    	</div>
    </section>             
	-->

    <?php $this->load->view('fragments/Footer.fragment.php') ?>
	
	<!-- jQuery -->
    <!--<script src="vendor/masterslider/jquery.min.js"></script>-->
    <script src="<?= BASE_URL().'public/' ?>vendor/masterslider/jquery.easing.min.js"></script>
     
    <!-- Master Slider -->
    <script src="<?= BASE_URL().'public/' ?>vendor/masterslider/masterslider.min.js"></script>
      
	
	<!-- owl Slider JavaScript -->
    <script src="<?= BASE_URL().'public/' ?>vendor/owlcarousel/dist/owl.carousel.min.js"></script>
    
    
    <!-- home-slider JavaScript -->
    <script src="<?= BASE_URL().'public/' ?>js/home-slider.js"></script>   
	<script>
		function Detail(id){
			var link = '<?= BASE_URL() ?>/Products/Detail/' + id;
			window.location.href = link;
		}

		function AddToCart(id){
			var link = '<?= BASE_URL() ?>/Products/Detail/' + id;
			$.post(link,
			{
				number_product: 1
			}, function(data, status){
						alert("<?= $this->l('Add to cart!') ?>");
					});
			
		}
	</script>
  </body>
</html>
