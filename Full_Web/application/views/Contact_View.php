<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= BASE_URL().'public/'?>images/<?= $setting['icon'] ?>">

    <title><?= $this->l('Contact') ?></title>

    <?php $this->load->view('fragments/Header.load.php') ?>

     
  </head>

  <body id="home">
  	
	<!-- loader start -->

	<div class="loader">
		<div id="awsload-pageloading">
			<div class="awsload-wrap">
				<ul class="awsload-divi">
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- loader end -->
    
    <?php $this->load->view('fragments/Topbar.fragment.php') ?>

    
    <!--Page Title-->
    
	<div class="page_title_ctn"> 
		<div class="container">
			<div class="row">                        
				<div class="col-sm-12">
					<div class="page-title clearfix">
						<h3>Contact</h3>
						<ol class="breadcrumb">
						  <li><a href="<?= BASE_URL() ?>">Home</a></li>
						  <li class="active"><span>Contact</span></li>
						</ol>
					</div>
				</div>
    		</div>           
    	</div>
    </div>
    
    <!-- Contact Section -->
    
    <section class="contactus-one" id="contactus"><!-- Section id-->
        <div class="container">
            <div class="row">
            	
            	<div class=" col-md-12 col-sm-12">
                	<div class="contact-block">
                        <div class="dart-headingstyle-one dart-mb-20">  <!--Style 1-->
                        <h2 class="dart-heading"><?= $this->l('Contact Us') ?></h2>
                      </div>
                        
                        <div class="contact-form"><!-- contact form -->
                            <form class="row" action="send_email.php" id="contact" name="contact" method="post" >
                              <div class="form-group col-md-6">
                                <input type="text" class="form-control" name="InputName" id="InputName" placeholder="Your Name" required >
                              </div>
                              <div class="form-group col-md-6">
                                <input type="email" class="form-control" name="InputEmail" id="InputEmail" placeholder="Your Email" required >
                              </div>
                              <div class="form-group col-md-12">
                                <input type="text" class="form-control" name="InputWeb" id="InputWeb" placeholder="Web Site (Optional)" >
                              </div>
                              <div class="form-group col-md-12">
                                <textarea class="form-control" name="InputMessage" id="InputMessage" rows="4" placeholder="Message" required  ></textarea>
                              </div>
                              <div class="col-md-12">
                              <button name="submit" type="submit" class="btn normal-btn dart-btn-xs"><?= $this->l('SEND MESSAGE') ?></button>
                              </div>
                            </form>
                        </div>
                        
                        <hr>
                        
                        <div class=" row contact-info">
                        	<div class="col-md-12 col-sm-12">
                        		<p class="addre"><i class="fa fa-map-marker"></i><?= $info['address'] ?></p>
                            </div>
                            <div class="col-md-12 col-sm-12">
                        		<p class="phone-call"><i class="fa fa-phone"></i> <a href="tel:<?= $info['phone'] ?>"><?= $info['phone'] ?></a></p>
                            </div>
                            <div class="col-md-12 col-sm-12">
                        		<p class="mail-id"><i class="fa fa-envelope"></i><a href="mailto:<?= $info['email'] ?>"><?= $info['email'] ?></a></p>
                            </div>
                        </div>
                        
					</div>
                </div>                
            </div>            
        </div>
    </section>            
                   
    <?php $this->load->view('fragments/Footer.fragment.php'); ?>

  </body>
</html>
