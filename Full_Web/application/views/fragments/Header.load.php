  <!-- Bootstrap core CSS -->
  <link href="<?= BASE_URL().'public/' ?>vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
           
           <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
           <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
           <script src="<?= BASE_URL().'public/' ?>js/ie-emulation-modes-warning.js"></script>
       
           <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
           <!--[if lt IE 9]>
             <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
             <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
           <![endif]-->
           
           <!-- Custom Fonts -->
           <link href="<?= BASE_URL().'public/' ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
          
           <!-- template CSS -->
           <link href="<?= BASE_URL().'public/' ?>css/style.css" rel="stylesheet">
             
           <!-- Custom CSS -->
           <link href="<?= BASE_URL().'public/' ?>css/custom.css" rel="stylesheet">
       