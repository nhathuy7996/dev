<div class="row">
    <div class="col-md-6 col-sm-6">
            <form  name="login_form" class="row dart-pt-20"  method="post">
                    <div class="col-sm-12">
                            <label for="Username">Username</label>
                            <input type="text" id="username" name="username" value="" class="dart-form-control" />
                    </div>

                    <div class="col-sm-12">
                            <label for="password">Password</label>
                            <input type="password" id="password" name="password" value="" class="dart-form-control" />
                    </div>

                    <div class="col-sm-12">
                            <input type="submit" value="Login" class="btn normal-btn dart-btn-xs" />
                    </div>

            </form>
    </div> 
</div>  