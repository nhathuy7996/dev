<?php 
 $query=$this->db->query("SELECT * FROM blog_theme order By id");
 $blogs_theme = $query->result_array();

 $query = $this->db->query("SELECT * FROM main_categories");
 $main_cate =  $query->result_array();

 $query = $this->db->query("SELECT * FROM setting");
 $setting =  $query->result_array()[0];

?>
<nav class="navbar navbar-default navbar-brand-top awesomenav">
   <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->
    <div class="container-fluid">  
		<div class="row"> 
			<div class="container"> 
				<!-- Start Atribute Navigation -->
				<div class="attr-nav left-side">
					<ul>
						<li><?= $setting['slogan'] ?></li>
					</ul>
				</div> 
				<div class="attr-nav">
					<ul><!--
						<li><a href="#">Login</a></li>
						<li><a href="#">Register</a></li>
						
						<li><a href="#">Wish List (0)</a></li>
						-->
					</ul>
					<ul>
						<!-- <li class="search"><a href="#"><i class="fa fa-search"></i></a></li> -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" >
								<i class="fa fa-shopping-bag"></i>
								<span class="badge"><?php if(isset($_SESSION['cart']) && !empty($_SESSION['cart'])) {
									$cart = json_decode($_SESSION['cart']);
									if( empty($cart)) echo 0; else echo count((array)$cart);
								} else echo 0; ?></span>
							</a>
							<ul class="dropdown-menu cart-list">
								<?php $total = 0; if(isset($cart) && !empty($cart)) foreach($cart as $c) { ?>
								<li>				
									<a href="<?= BASE_URL().'Products/Detail/'.$c->id_product ?>" class="photo">
										<img src="<?= BASE_URL().'public/' ?>images/product/details/<?= $c->id_product ?>/<?= json_decode( $c->image)[0] ?>" class="cart-thumb" alt="" /></a>
									<h6><a href="<?= BASE_URL().'Products/Detail/'.$c->id_product ?>">
											<?= $c->name ?></a></h6>
									<p><?= $c->quantity ?>x - <span class="price"><?= $c->sell_price ?> VNĐ</span></p>
								</li>
								<?php $total += $c->quantity * $c->sell_price;  } ?>
								<li class="total">
									<span class="pull-right"><strong>Total: </strong><?= $total ?> VNĐ</span>
									<a href="<?= BASE_URL().'Cart' ?>" class="btn btn-default btn-cart">Cart</a>
								</li>
							</ul>
						</li>
						<li class="side-menu"><a href="#"><i class="fa fa-bars"></i></a></li>
					</ul>
				</div>        
				<!-- End Atribute Navigation -->


				<!-- Start Header Navigation -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
						<i class="fa fa-bars"></i>
					</button>
					<a class="navbar-brand" href="<?= BASE_URL() ?>"><img src="<?= BASE_URL().'public/' ?>images/<?= $setting['logo'] ?>" class="logo" alt=""></a>
				</div>
				<!-- End Header Navigation -->
			</div> 
			<div class="border-menu">
				<div class="container"> 
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="navbar-menu">
						<ul class="nav navbar-nav nav-justified" data-in="fadeInDown" data-out="fadeOutUp">
							<li class="active"><a href="<?= base_url() ?>">Home</a></li>   
							<li class="dropdown megamenu-fw">
								<a href="<?= BASE_URL().'Products' ?>" class="dropdown-toggle" data-toggle="dropdown">categories</a>
								<ul class="dropdown-menu megamenu-content" role="menu">
									<li>
										<div class="row">
											<?php foreach($main_cate as $m){ ?>
											<div class="col-menu col-md-3">
												<h5 class="title"><?= $m['name'] ?></h5>
												<div class="content">
													<ul class="menu-col">
														<?php $id_cate = $m['id_cate'];
														 	$query = $this->db->query("SELECT * FROM sub_categories WHERE id_main_cate = $id_cate");
															  $sub_cate = $query->result_array();
															  foreach($sub_cate as $s){ ?>
																<li><a href="<?= BASE_URL().'Products/'.$s['id_sub_cate'] ?>"><?= $s['name']?></a></li>
															  <?php } ?>
													</ul>
												</div>
											</div><!-- end col-3 -->
											<?php } ?>
										</div><!-- end row -->
									</li>
								</ul>
							</li>

							<li class="dropdown">
								<a href="<?= BASE_URL().'Blog' ?>" class="dropdown-toggle" data-toggle="dropdown" >Blog</a>
								<ul class="dropdown-menu">
									<?php foreach($blogs_theme as $b){ ?>
									<li><a href="<?= BASE_URL().'Blog/'.$b['id'] ?>"><?= $b['name'] ?></a></li>
									<?php } ?>
								</ul>
							</li>

							<li><a href="<?= BASE_URL().'Contact' ?>">CONTACT</a></li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div> 
			</div>
		</div> 
    </div> 
    
    <!-- Start Side Menu -->
	<div class="side">
		<a href="#" class="close-side"><i class="fa fa-times"></i></a>
		<div class="widget">
			<ul class="link">
				<?php if(isset($_SESSION['userid']) && !empty($_SESSION['userid'])) { ?>
					<li><a href="<?= BASE_URL() ?>User"><?= $this->l("My Account") ?></a></li>
					<li><a href="<?= BASE_URL() ?>LogOut"><?= $this->l("SignOut") ?></a></li>
				<?php } else { ?>
					<li><a href="<?= BASE_URL() ?>User"><?= $this->l("Signin") ?></a></li>
					<li><a href="<?= BASE_URL() ?>SignUp"><?= $this->l("SignUp") ?></a></li>
				<?php } ?>
				<li><a href="<?= BASE_URL() ?>Cart"><?= $this->l("Cart") ?></a></li>
				<li><a href="<?= BASE_URL() ?>PrivacyPolicy"><?= $this->l("Policy") ?></a></li>
			</ul>
		</div>
	</div>
	<!-- End Side Menu -->
	
	</nav>