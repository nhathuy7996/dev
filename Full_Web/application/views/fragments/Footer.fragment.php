<?php
 $query = $this->db->query("SELECT * FROM information");
 $info =  $query->result_array()[0];

 $query = $this->db->query("SELECT * FROM html_pages");
 $Pages_html =  $query->result_array();
?>
<footer class="footer">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-4 col-sm-4">
    				<div class="footer-block about">
						<h4 class="footer-title">Company</h4><hr class="footer_border">
						<ul class="list-unstyled">
						  <li><a href="<?= BASE_URL().'AboutUs' ?>">About Us</a></li>
						  <li><a href="<?= BASE_URL().'Contact' ?>">Contact Us</a></li>
						  <li><a href="<?= BASE_URL().'PrivacyPolicy' ?>">Privacy Policy</a></li>
						  <li><a href="<?= BASE_URL().'TermsConditions' ?>">Terms &amp; Conditions</a></li>
						  <li><a href="javascript:void(0)">Support Centre</a></li>
					   </ul>
    				</div>
    			</div>
				<div class="col-md-4 col-sm-4">
    				<div class="footer-block twitter">
						<h4 class="footer-title">Questions?</h4><hr class="footer_border">
						<ul class="list-unstyled">
							<?php foreach($Pages_html as $p) { ?>
								<li><a href="<?= BASE_URL().'Info/'.$p['id'] ?>"><?= $this->l($p['name']) ?></a></li>
							<?php }?>
						</ul>
    				</div>
    			</div>
				<div class="col-md-4 col-sm-4">
    				<div class="footer-block contact">
						<h4 class="footer-title">Connect With Us</h4><hr class="footer_border">
						<div class="contact_footer">
						  <div class="footer_icon">
							 <a><i class="fa fa-facebook" aria-hidden="true"></i></a>
							 <a><i class="fa fa-twitter" aria-hidden="true"></i></a>
							 <a><i class="fa fa-instagram" aria-hidden="true"></i></a>
							 <a><i class="fa fa-pinterest" aria-hidden="true"></i></a>
							 <a><i class="fa fa-youtube" aria-hidden="true"></i></a>
							 <a><i class="fa fa-linkedin" aria-hidden="true"></i></a>
							 <a><i class="fa fa-google-plus" aria-hidden="true"></i></a>
						  </div>
					   </div>
						<br>
						<div class="contact_footer">
						  <div class="footer_icon">
							 <div><i class="fa fa-paper-plane" aria-hidden="true"></i><?= $info['address'] ?></div>
							 <div><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:<?= $info['email'] ?>"><?= $info['email'] ?></a></div>
							 <div><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:<?= $info['phone'] ?>"><?= $info['phone'] ?></a></div>
						  </div>
					   </div>
    				</div>
    			</div>
    		</div>
    	</div>
		<div class="footer_bottom_outer">
		  <div class="container">
			 <div class="row">
			 	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 footerbottom_link">
				   <div class="card">
					  
				   </div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				   <div class="copyright">
					  <p>Created by <a href="https://www.facebook.com/minh.hang.3011">HangNM</a></p>
				   </div>
				</div>            
			 </div>
		  </div>
	   </div>
    </footer>     

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?= BASE_URL().'public/' ?>js/ie10-viewport-bug-workaround.js"></script>
    
    <!-- jQuery -->
    <script src="<?= BASE_URL().'public/' ?>vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?= BASE_URL().'public/' ?>vendor/bootstrap/js/bootstrap.min.js"></script>
    	
    <!-- Nav JavaScript -->
    <script src="<?= BASE_URL().'public/' ?>js/awesomenav.js"></script>    
	    
    <!-- custom JavaScript -->
    <script src="<?= BASE_URL().'public/' ?>js/custom.js"></script> 
    
    <!-- template JavaScript -->
    <script src="<?= BASE_URL().'public/' ?>js/template.js"></script>