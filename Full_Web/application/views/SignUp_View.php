<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= BASE_URL().'public/'?>images/<?= $setting['icon'] ?>">

    <title><?= $this->l('Signup') ?></title>

    <?php $this->load->view('fragments/Header.load.php') ?>

		<script src="https://sdk.accountkit.com/en_US/sdk.js"></script>
  </head>

  <body id="home">
  <script>
          //https://developers.facebook.com/docs/accountkit/webjs
          console.log("<p>initialized Account Kit.</p>");
          
          // initialize Account Kit with CSRF protection
          AccountKit_OnInteractive = function(){
            AccountKit.init(
              {
                appId:"1961581170794249", 
                state:"vananhduong", 
                version:"v2.1",
                fbAppEventsEnabled:true
              }
            );
          };
            
          // login callback
          function loginCallback(response) {
            if (response.status === "PARTIALLY_AUTHENTICATED") {
              var code = response.code;
              var csrf = response.state;
                             
                $.post("<?= BASE_URL().'SignUpVer' ?>", { code : code, csrf : csrf }, function(result){
										$("#SignUp_Form").submit();
                });
                
            }
            else if (response.status === "NOT_AUTHENTICATED") {
              // handle authentication failure
							$("#phone").attr("disabled","false");
                console.log("<p>( Error ) NOT_AUTHENTICATED status received from facebook, something went wrong.</p>");
            }
            else if (response.status === "BAD_PARAMS") {
              // handle bad parameters
							$("#phone").attr("disabled","false");
                console.log("<p>( Error ) BAD_PARAMS status received from facebook, something went wrong.</p>");
            }
          }
            
            
          // phone form submission handler
          function smsLogin() {
            var countryCode = "+84";
            var phoneNumber = $("#phone").val();
            console.log("<p>Triggering phone validation.</p>");
            AccountKit.login(
              'PHONE', 
              {countryCode: countryCode, phoneNumber: phoneNumber}, // will use default values if not specified
              loginCallback
            );

          }

        </script>
	<!-- loader start -->

	<div class="loader">
		<div id="awsload-pageloading">
			<div class="awsload-wrap">
				<ul class="awsload-divi">
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- loader end -->
    
    <?php $this->load->view('fragments/Topbar.fragment.php') ?>

    
    <!--Page Title-->
    
	<div class="page_title_ctn"> 
		<div class="container">
			<br>
			<?php if(!empty($errors)) foreach($errors as $e){ ?>
				<h6 style="color: red"><?= $this->l($e) ?></h6>
			<?php } ?>
			<form action="<?= BASE_URL() ?>SignUpVer" method="POST" id="SignUp_Form">
				<div class="col-sm-12">
						<label for="username"><?= $this->l("Username") ?>:</label>
						<input type="text" id="username" name="username" value="" class="dart-form-control" required />
				</div>
				<div class="col-sm-12">
						<label for="pass"><?= $this->l("Password") ?>:</label>
						<input type="password" id="password" name="password" value="" class="dart-form-control" required />
				</div>

				<div class="col-sm-12">
						<label for="pass"><?= $this->l("Confirm Password") ?>:</label>
						<input type="password" id="cf_pass" name="cf_pass" value="" class="dart-form-control" required />
				</div>
				<div class="col-sm-12">
						<label for="phone"><?= $this->l("Phone number") ?>:</label>
						<input type="text" id="phone" name="phone" value="" class="dart-form-control" required />
				</div>
				<div class="col-sm-12">
						<input type="button" onclick="smsLogin()"  value="<?= $this->l("Sign Up") ?>" class="btn normal-btn dart-btn-xs" />
				</div>
			</form>	         
		</div>
	</div>         
                   
    <?php $this->load->view('fragments/Footer.fragment.php'); ?>
		
		
  </body>
</html>
