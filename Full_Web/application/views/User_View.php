<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= BASE_URL().'public/'?>images/<?= $setting['icon'] ?>">

    <title><?= $this->l('Account') ?></title>

    <?php $this->load->view('fragments/Header.load.php') ?>

     
  </head>

  <body id="home">
  	
	<!-- loader start -->

	<div class="loader">
		<div id="awsload-pageloading">
			<div class="awsload-wrap">
				<ul class="awsload-divi">
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- loader end -->
    
    <?php $this->load->view('fragments/Topbar.fragment.php') ?>

    
    <!--Page Title-->
    
	<div class="page_title_ctn"> 
		<div class="container">
			<div class="row">                        
				<div class="col-sm-12">
					<div class="page-title clearfix">
						<h3><?= $this->l('Account') ?></h3>
					</div>
				</div>
    		</div>           
    	</div>
    </div>
    
    <!-- Contact Section -->
    
    <section class="contactus-one" id="contactus"><!-- Section id-->
        <div class="container">
				<div class="row">
				<?php if(!isset($_SESSION['userid']) || empty($_SESSION['userid'])){ ?>   
				<?php $this->load->view('fragments/User_Login_View'); } else { ?>
					<form action="<?= BASE_URL().'UserUpdate' ?>"  method="post" enctype="multipart/form-data" >

					<div class="col-sm-6">
							<img id="avatar_preview" width="150" height="150" class = "thumb"  src = "<?= BASE_URL().'public/'?>images/avatars/<?= $userdata['avatar'] ?>" >
							<input type="file" id="avatar" name="avatar" class="form-control-file">
					</div>
					<div class="col-sm-6">
							<label for="firstname">First Name:</label>
							<input type="text" id="firstname" name="firstname" value="<?= $userdata['firstname'] ?>" class="dart-form-control" />
					</div>

					<div class="col-sm-6 col_last">
							<label for="lastname">Last Name:</label>
							<input type="text" id="lastname" name="lastname" value="<?= $userdata['lastname'] ?>" class="dart-form-control" />
					</div>

					<div class="clear"></div>

					<div class="col-sm-12">
							<label for="phone">Phone Number:</label>
							<input type="text" id="phone" name="phone" value="<?= $userdata['phone'] ?>" class="dart-form-control" />
					</div>

					<div class="col-sm-12">
							<label for="address1">Address:</label>
							<input type="text" id="address1" name="address1" value="<?= $userdata['address'] ?>" class="dart-form-control" />
					</div>

					<div class="col-sm-12">
							<input type="text" id="address2" name="address2" value="<?= $userdata['address2'] ?>" class="dart-form-control" />
					</div>
					<div class="col-sm-12">
							<input type="submit" value="Save" class="btn normal-btn dart-btn-xs" />
					</div>
					</form>
					<form action="<?= BASE_URL() ?>UserPass" method="post">
						<div class="col-sm-12">
								<label for="address1"><?= $this->l("Password") ?>:</label>
								<input type="password" id="pass" name="pass" value="" class="dart-form-control" required />
						</div>
						<div class="col-sm-12">
								<label for="address1"><?= $this->l("New Password") ?>:</label>
								<input type="password" id="new_pass" name="new_pass" value="" class="dart-form-control" required />
						</div>

						<div class="col-sm-12">
								<label for="address1"><?= $this->l("Confirm New Password") ?>:</label>
								<input type="password" id="cf_new_pass" name="cf_new_pass" value="" class="dart-form-control" required />
						</div>
						<div class="col-sm-12">
								<input type="submit"  value="<?= $this->l("Change pass") ?>" class="btn normal-btn dart-btn-xs" />
						</div>
					</form>
				<?php } ?>
				</div>
        </div>
    </section>            
                   
    <?php $this->load->view('fragments/Footer.fragment.php'); ?>
		
		<script>
		function handleFileSelect(evt) {
			var files = evt.target.files; // FileList object

			// Loop through the FileList and render image files as thumbnails.
			for (var i = 0, f; f = files[i]; i++) {

			// Only process image files.
			if (!f.type.match('image.*')) {
					continue;
			}

			var reader = new FileReader();

			// Closure to capture the file information.
			reader.onload = (function(theFile) {
					return function(e) {
					// Render thumbnail.
					document.getElementById('avatar_preview').setAttribute("src", e.target.result);
					};
			})(f);

			// Read in the image file as a data URL.
			reader.readAsDataURL(f);
			}
		}
		document.getElementById('avatar').addEventListener('change', handleFileSelect, false);

		</script>
  </body>
</html>
