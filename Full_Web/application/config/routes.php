<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home_Controller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['Admin'] = 'Admin_Controller';
$route['Admin/Slider'] = 'Admin_Controller/Slider';
$route['Admin/Promo'] = 'Admin_Controller/Promo';
$route['Admin/Partner'] = 'Admin_Controller/Partner_Image';
$route['Admin/Blog'] = 'Admin_Blog_Controller/Blog';
$route['Admin/Blog/(:any)'] = 'Admin_Blog_Controller/Blog/$1';
$route['Admin/Blog/(:any)/Page/(:any)'] = 'Admin_Blog_Controller/Blog/$1/$2';
$route['Admin/Blog/Page/(:any)'] = 'Admin_Blog_Controller/Blog//$1';
$route['Admin/BlogTheme'] = 'Admin_Blog_Controller/Theme';

$route['Admin/Products'] = 'Admin_Product_Controller/Product';
$route['Admin/Products/(:any)'] = 'Admin_Product_Controller/Product/$1';
$route['Admin/Products/(:any)/Page/(:any)'] = 'Admin_Product_Controller/Product/$1/$2';
$route['Admin/Products/Page/(:any)'] = 'Admin_Product_Controller/Product//$1';
$route['Admin/Products/(:any)/Page/(:any)'] = 'Admin_Product_Controller/Product/$1/$2';

$route['Admin/MainCate'] = 'Admin_Product_Controller/MainCategories';
$route['Admin/SubCate'] = 'Admin_Product_Controller/SubCategories';

$route['Admin/Orders'] = 'Admin_Order_Controller';

$route['Admin/Setting'] = 'Admin_Setting_Controller';
$route['Admin/Info'] = 'Admin_Setting_Controller/Infomation';
$route['Admin/Condition'] = 'Admin_Setting_Controller/Condition';
$route['Admin/Logo'] = 'Admin_Setting_Controller/Logo';

$route['Products'] = 'Product_Controller';
$route['Products/Page/(:any)'] = 'Product_Controller/index/$1';
$route['Products/(:any)'] = 'Product_Controller/Catalog/$1';
$route['Products/(:any)/Page/(:any)'] = 'Product_Controller/Catalog/$1/$2';
$route['Products/Detail/(:any)'] = 'Product_Controller/Product_Detail/$1';

$route['Blog'] = 'Blog_Controller';
$route['Blog/Page/(:any)'] = 'Blog_Controller/Theme//$1';
$route['Blog/(:any)'] = 'Blog_Controller/Theme/$1';
$route['Blog/(:any)/Page/(:any)'] = 'Blog_Controller/Theme/$1/$2';
$route['Blog/Detail/(:any)'] ='Blog_Controller/Detail/$1';
$route['Order'] = 'Product_Controller/Order';

$route['Cart'] = 'Cart_Controller';

$route['AboutUs'] = 'Home_Controller/About';
$route['Contact'] = 'Home_Controller/Contact';
$route['TermsConditions'] = 'Home_Controller/Term';
$route['PrivacyPolicy'] = 'Home_Controller/Policy';

$route['User'] = 'User_Controller';
$route['LogOut'] = 'User_Controller/Logout';
$route['UserUpdate'] = 'User_Controller/Data_Update';
$route['UserPass'] = 'User_Controller/Pass_Update';
$route['SignUp'] = 'User_Controller/SignUp';
$route['SignUpVer'] = 'User_Controller/SignUp_verify';

$route['Info/(:any)'] = 'Html_Page_Controller/LoadPage/$1';
$route['Admin/HtmlInfor'] = 'Html_Page_Controller/AddPage';
