<?php
class MY_Controller extends CI_Controller{
    function __construct (){
        parent :: __construct();
        $default_lan =  explode(",",$_SERVER['HTTP_ACCEPT_LANGUAGE']);

        $this->load->model("Setting_Model");
        $lan = $this->Setting_Model->Language();   
        
        for($i =0;$i < count($default_lan); $i++){
            if(in_array($default_lan[$i],$lan)){
                $this->lang->load($default_lan[$i], "vietnamese");
                return;
            }
        }

        $this->lang->load("en", "vietnamese");
    }

    public function CheckUser(){
        if(!isset($_SESSION['userid']) || empty($_SESSION['userid']) ){
            if(!isset($_POST['username'])){
                return false;
            }else
                return $this->Login();
        }
        return true;
    }

    function Login(){
        $this->load->model('Users_Model');
        $name = $_POST['username'];
        $pass = $_POST['password'];
        $user_id = $this->Users_Model->CheckUser($name,$pass);
        if($user_id != false){
            $_SESSION['userid'] = $user_id;
            return true;
        }
        return false;
    }

    public function CheckAdmin(){
        if(!isset($_SESSION['IsAdmin']) || empty($_SESSION['IsAdmin']) || $_SESSION['IsAdmin'] == false ){
            if(!isset($_POST['name']))
                $this->load->view("Admin/Login_View");
            else
                $this->AdminLogin();
        }else
            return true;
    }

    public function AdminLogin(){
        $this->load->model('Users_Model');
        $name = $_POST['name'];
        $pass = $_POST['password'];

        if( $this->Users_Model->IsAdmin($name,$pass) ){
            $_SESSION['IsAdmin'] = true;
            $this->load->view("Admin/Admin_Header_View");
            $this->load->view("Admin/fragments/Admin_Body_View");
            $this->load->view("Admin/Admin_Footer_View");
            return true;
        }else{
            $this->load->view("Admin/Login_View");
            return false;
        }
    }
}

?>