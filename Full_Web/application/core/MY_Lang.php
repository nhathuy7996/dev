<?php
class MY_Lang extends CI_Lang{
    function __construct(){
        parent::__construct();
    }

    function line($line, $log_errors = TRUE)
	{
		$value = isset($this->language[$line]) ? $this->language[$line] : FALSE;

		// Because killer robots like unicorns!
		if ($value === FALSE && $log_errors === TRUE)
		{
			return $line;
		}

		return $value;
	}
}

?>