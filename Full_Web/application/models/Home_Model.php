<?php
class Home_Model extends CI_Model{
    function Get_Slider(){
        $sql = "SELECT * FROM slide_image";
        $query  = $this->db->query($sql);

        return $query->result_array();
    }

    function Del_Slider($id =  1){
        $sql = "SELECT * FROM slide_image WHERE id_slide=$id";
        $query  = $this->db->query($sql);

        if($query->num_rows()<1)
            return false;
        $image = $query->result_array()[0];

        
        $link = 'public/images/slide/'.$image['link'];

        if( unlink($link)){
            $sql = "DELETE FROM slide_image WHERE id_slide=$id ";
            $query  = $this->db->query($sql);
        }         
    }

    function Add_Slider($name = '',$setting = ''){
        $sql = "INSERT INTO slide_image (link,alt,setting) VALUES ('$name','$name','$setting')";
        $query  = $this->db->query($sql);
    }

    function Get_Promo(){
        $sql = "SELECT * FROM promo_image";
        $query  = $this->db->query($sql);

        return $query->result_array();
    }

    function Del_Promo($id =  1){
        $sql = "SELECT * FROM promo_image WHERE id=$id";
        $query  = $this->db->query($sql);

        if($query->num_rows()<1)
            return false;
        $image = $query->result_array()[0];

        
        $link = 'public/images/promo/'.$image['link'];

        if( unlink($link)){
            $sql = "DELETE FROM promo_image WHERE id=$id ";
            $query  = $this->db->query($sql);
        }         
    }

    function Add_Promo($name = ''){
        $sql = "INSERT INTO promo_image (link,alt) VALUES ('$name','$name')";
        $query  = $this->db->query($sql);
    }

    function Get_Partner_Image(){
        $sql = "SELECT * FROM partner_image";
        $query  = $this->db->query($sql);

        return $query->result_array();
    }

    function Del_Partner_Image($id =  1){
        $sql = "SELECT * FROM partner_image WHERE id=$id";
        $query  = $this->db->query($sql);

        if($query->num_rows()<1)
            return false;
        $image = $query->result_array()[0];

        
        $link = 'public/images/partner/'.$image['link'];

        if( unlink($link)){
            $sql = "DELETE FROM partner_image WHERE id=$id ";
            $query  = $this->db->query($sql);
        }         
    }

    function Add_Partner_Image($name = ''){
        $sql = "INSERT INTO partner_image (link,alt) VALUES ('$name','$name')";
        $query  = $this->db->query($sql);
    }

    function Get_Blog(){
        $sql = "SELECT * FROM post ORDER BY id_post DESC LIMIT 3";
        $query  = $this->db->query($sql);

        return $query->result_array();
    }
}
?>