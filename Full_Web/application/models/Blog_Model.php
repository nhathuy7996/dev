<?php
class Blog_Model extends CI_Model{
   

    public function Get_All_Theme(){      
        $query=$this->db->query("SELECT * FROM blog_theme order By id");

        return $query->result_array();
    }

    public function Get_Theme_Blog($id = ''){
        if($id == ''){
            $r['id'] = '';
            return $r;
            
        }else{
            $query=$this->db->query("SELECT * FROM blog_theme WHERE id=$id");
        }

        if($query->num_rows() > 0)
            return $query->result_array()[0];

        $r['id'] = $id;
        return $r;
    }

    function Add_Theme($name = ''){
        $sql = "INSERT INTO blog_theme ( name) VALUES ('$name')";
        $query = $this->db->query($sql);
    }

    function Edit_Theme($id = '', $name = ''){
        $sql = "UPDATE blog_theme SET name = '$name' WHERE id = $id";
        $query = $this->db->query($sql);
    }

    function Delete_Theme($id = ''){
        $sql = "DELETE FROM blog_theme WHERE  id = $id";
        $query = $this->db->query($sql);
    }

    public function Get_List_Post($theme_id = '',$page = 1){
        
        $limit = 6;
        if($page > $this->Count_Page($theme_id))
            $limit = ( $this->Count_Page($theme_id) * 6) % 6;

        $page *= 6;

        if($theme_id == ''){
            $sql = "SELECT  * FROM (SELECT * FROM post ORDER BY id_post DESC LIMIT $page ) AS T
                ORDER BY id_post LIMIT $limit ";
        }else{
            $sql = "SELECT * FROM (SELECT * FROM post WHERE theme LIKE '%\"$theme_id\"%' ORDER BY id_post DESC LIMIT $page) AS T  
             ORDER BY id_post LIMIT $limit";
        }

        $query =  $this->db->query("SELECT * FROM ($sql) AS T ORDER BY id_post DESC ");

        return $query->result_array();
    }

    function Count_Page($theme_id = ''){
        if($theme_id == ''){
            $query=$this->db->query("SELECT * FROM post order By id_post DESC");
            
        }else{
            $query=$this->db->query("SELECT * FROM post WHERE theme LIKE '%\"$theme_id\"%' order By id_post DESC");
        }

        return $query->num_rows()/6;
    }

    public function Get_Post($post_id = ''){
       
        $query=$this->db->query("SELECT * FROM post WHERE id_post = $post_id");

        return $query->result_array()[0];
    }

    public function Add_Post($picture = '',$title='',$summary = '',$author = '', $theme = '', $content = ''){
        $t=time(); 
        $t = date("Y-m-d",$t);  
        $sql = "INSERT INTO post (picture,title, summary, author, theme,content,date_create) VALUES ('$picture' ,'$title','$summary' ,'$author' , '$theme' , '$content','$t'  )";

        $this->db->query($sql);
    }

    public function Update_Post($id = '',$title='',$summary = '',$author = '', $theme = '', $content = ''){
        $sql = "UPDATE post SET title='$title',summary='$summary',author='$author',theme='$theme',content='$content' WHERE id_post = $id";
        $this->db->query($sql);
    }

    public function Delete_Post($id = ''){
        $sql = "DELETE FROM post  WHERE id_post = $id";
        $this->db->query($sql);
    }
}
?>