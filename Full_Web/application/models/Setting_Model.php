<?php
class Setting_Model extends CI_Model{
    function Get_all_setting(){
        $sql= "SELECT * FROM setting";
        $query = $this->db->query($sql);

        return $query->result_array()[0];
    }

    function Get_Information(){
        $sql= "SELECT * FROM information";
        $query = $this->db->query($sql);

        return $query->result_array()[0];
    }

    function Language($lan = ''){
        if(empty($lan)){
            $sql= "SELECT language FROM setting";
            $query = $this->db->query($sql);
    
            return $query->result_array();
        }
        
    }

    function Update_Setting($name ='',$slogan = '',$intro ='',$language = '',$icon ='',$IntroImage = '',$logo = ''){
        $sql = "UPDATE setting SET name_web = '$name', icon='$icon',slogan='$slogan',intro='$intro',intro_image='$IntroImage',language='$language',logo='$logo' WHERE 1";
        $query = $this->db->query($sql);
    }

    function UpdateInfo($address = '',$phone = '',$email = '', $policy = '', $term = '', $ship = '', $about = ''){
        $sql = "UPDATE information SET address = '$address', phone='$phone', email='$email', policy='$policy', term ='$term',shipping='$ship', about='$about' WHERE 1";
        $query = $this->db->query($sql);
    }
}
?>