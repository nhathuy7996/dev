<?php
class Product_Model extends CI_Model{

    function Get_List_Product($id_catalog = '',$page = 1){
        $limit = 12;
        if($page > $this->Count_Page($id_catalog))
            $limit = ( $this->Count_Page($id_catalog) * 12) % 12;

        $page *= 12;

        if($id_catalog == ''){
            $sql = "SELECT  * FROM (SELECT * FROM product ORDER BY id_product DESC LIMIT $page ) AS T
                ORDER BY id_product LIMIT $limit ";
        }else{
            $sql = "SELECT * FROM (SELECT * FROM product WHERE catalogs LIKE '%\"$id_catalog\"%' ORDER BY id_product DESC LIMIT $page) AS T  
             ORDER BY id_product LIMIT $limit";
        }

        $query =  $this->db->query("SELECT * FROM ($sql) AS T ORDER BY id_product DESC ");

        return $query->result_array();
    }

    function Count_Page($id_catalog = ''){
        if($id_catalog == ''){
            $sql = "SELECT * FROM product";
        }else{
            $sql = "SELECT * FROM product WHERE catalogs LIKE '%\"$id_catalog\"%'";
        }

        $query =  $this->db->query($sql);

        return $query->num_rows()/12;
    }

    function Get_NewProduct(){
        $sql = "SELECT * FROM product ORDER BY id_product DESC LIMIT 5";
        $query =  $this->db->query($sql);

        return $query->result_array();
    }

    function Get_Detail_Product($id = ''){

        if($id == 'add' && isset($_SESSION['IsAdmin']) && $_SESSION['IsAdmin'] == true ){
            //$sql = "INSERT INTO product (catalogs,image) VALUES ('[]','[]')";
            //$query = $this->db->query($sql);
            
            //$sql = "SELECT * FROM product ORDER BY id_product DESC";
            //$query = $this->db->query($sql);

            //header("Location: ".BASE_URL().'Admin/Products?id='.$query->result_array()[0]['id_product']);
            //die();

            return null;
        }

        if(is_numeric($id)){
            $sql = "SELECT * FROM product WHERE id_product = $id";
            $query = $this->db->query($sql);
    
            return $query->result_array()[0];
        }

        $sql = "SELECT * FROM product ORDER BY id_product DESC";
        $query = $this->db->query($sql);

        return $query->result_array()[0];
    }

    function Add_Product($name = '', $catalogs = '', $price = '',$sell_price = '',$image = '',$description = '',$quantity = 0){
        $sql = "INSERT INTO product (name, catalogs, price, sell_price, image, description,Quantity) 
        VALUES ('$name','$catalogs',$price,$sell_price,'$image','$description',$quantity)";

        $this->db->query($sql);
        header("Location: ".BASE_URL().'Admin/Products');
        die();
    }

    function Delete_Product($id = ''){
        $sql = "DELETE FROM product WHERE id_product = $id";
        $query = $this->db->query($sql);

    }

    function Get_List_Main_Cate(){
        $sql = "SELECT * FROM main_categories";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function Add_Main_Cate($name = ''){
        $sql = "INSERT INTO main_categories ( name) VALUES ('$name')";
        $query = $this->db->query($sql);
    }

    function Edit_Main_Cate($id = '', $name = ''){
        $sql = "UPDATE main_categories SET name = '$name' WHERE id_cate = $id";
        $query = $this->db->query($sql);
    }

    function Delete_Main_Cate($id = ''){
        $sql = "DELETE FROM main_categories WHERE  id_cate = $id";
        $query = $this->db->query($sql);
    }

    function Get_Sub_Cate($id_main_cate = ''){
        if($id_main_cate == ''){
            $sql = "SELECT * FROM sub_categories";
        }else{
            $sql = "SELECT * FROM sub_categories WHERE id_main_cate = $id_main_cate";
        }

        $query =  $this->db->query($sql);

        return $query->result_array();
    }

    function Add_Sub_Cate($name = '',$main_cate = 1){
        $sql = "INSERT INTO sub_categories ( name, id_main_cate) VALUES ('$name',$main_cate)";
        $query = $this->db->query($sql);

        var_dump($name);
    }

    function Edit_Sub_Cate($id = '', $name = ''){
        $sql = "UPDATE sub_categories SET name = '$name' WHERE id_sub_cate = $id";
        $query = $this->db->query($sql);
    }

    function Delete_Sub_Cate($id = ''){
        $sql = "DELETE FROM sub_categories WHERE  id_sub_cate = $id";
        $query = $this->db->query($sql);
    }

    public function Update_Product($id = '',$name ='', $catalogs='', $price = '', $sell_price='', $image='', $description='',$quantity = 0){
        $sql = "UPDATE product SET name = '$name',catalogs='$catalogs',price='$price',sell_price='$sell_price',image='$image',description='$description',Quantity=$quantity WHERE id_product=$id";
        $this->db->query($sql);
    }

}
?>