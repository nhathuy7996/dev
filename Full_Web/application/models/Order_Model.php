<?php
class Order_Model extends CI_Model{
    function Get_Order_pending(){
        $sql="SELECT * FROM orders WHERE status_order = 'pending' ORDER BY id_order DESC";
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function Get_All_Orders(){
        $sql="SELECT * FROM orders ORDER BY id_order DESC";
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function SetOrder_Status($id, $status){
        $sql="UPDATE orders SET status_order = '$status' WHERE id_order = $id ";
        $query = $this->db->query($sql);
    }
}

?>