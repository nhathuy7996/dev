<?php
class Cart_Model extends CI_Model{
    public function Place_Order($name='',$lname='',$cname='',$addrs1='',
        $addrs2 = '', $city='', $note='', $cart=''){
        
            $t=time(); 
            $t = date("Y-m-d",$t);

            $userid = "";
            if(isset($_SESSION['userid'])){
                $userid = $_SESSION['userid'];
            }

            $data = array(
                'name' => $name,
                'user_id' => $userid,
                'last_name' => $lname,
                'comp_name' => $cname,
                'address1' => $addrs1,
                'address2' => $addrs2,
                'city' => $city,
                'note' => $note,
                'cart' => $cart,
                'date_create' => $t,
                'status_order' => 'pending'
        );
        $this->db->insert('orders', $data);

    }
}

?>