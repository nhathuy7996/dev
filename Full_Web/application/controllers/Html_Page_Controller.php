<?php
class Html_Page_Controller extends MY_Controller{
    function LoadPage($id = ''){
        $data = array();

        $this->load->model('Html_Model');
        $data['body'] = $this->Html_Model->Get_page($id)[0];

        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();
        $data['info'] = $this->Setting_Model->Get_Information();

        $this->load->view('Html_Page_View',$data);
    }

    function AddPage(){
        if (!parent::CheckAdmin()) return;

        $data = array();
        
        $this->load->model('Html_Model');

        if(isset($_POST['EditHtml'])){
            $this->Html_Model->Edit_page($_POST['EditHtml'],$_POST['name'],$_POST['body']);
        }

        if(isset($_POST['AddHtml'])){
            $this->Html_Model->Add_page($_POST['name'],$_POST['body']);
        }

        $data['body'] = $this->Html_Model->Get_page();

        $this->load->view("Admin/Admin_Header_View");
        $this->load->view("Admin/fragments/Html_Page/Admin_Html_View",$data);

        $this->load->view("Admin/Admin_Footer_View");
    }
}

?>