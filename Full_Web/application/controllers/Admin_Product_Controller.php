<?php

class Admin_Product_Controller extends MY_Controller{
    function Product($Cate_id = '',$page = 1){

        if (!parent::CheckAdmin()) return;

        $this->load->view("Admin/Admin_Header_View");
        $data = array();

        $this->load->model("Product_Model");
        $data['main_cate'] = $this->Product_Model->Get_List_Main_Cate();            
        $data['sub_cate'] = $this->Product_Model->Get_Sub_Cate();

        if(isset($_POST['id_product']) && !empty($_POST['id_product'])){
            if($_POST['id_product'] == "new")
                $this->AddProduct();
            else
                $this->Edit_Product();
        }

        if(isset($_GET['img']) && !empty($_GET['img'])){
            $this->Edit_Product();
        }

        if(isset($_GET['Del']) && !empty($_GET['Del'])){
            $this->Product_Model->Delete_Product($_GET['Del']);
            $this->deleteDir("public/images/product/details/".$_GET['Del']);
        }

        if(isset($_GET['id']) && !empty($_GET['id'])){
            $data['product'] =  $this->Product_Model->Get_Detail_Product($_GET['id']);
            $this->load->view('Admin/fragments/Product/Admin_ProductDetail_View',$data);
        }else{
            
            $data['Products'] = $this->Product_Model->Get_List_Product($Cate_id,$page);
            $data['pages_number'] = $this->Product_Model->Count_Page($Cate_id);
            $data['page'] = $page;
            $data['catalog'] = $Cate_id;

            $this->load->view('Admin/fragments/Admin_Product_View',$data);
        }
        
        $this->load->view("Admin/Admin_Footer_View");
       
    }

    function AddProduct(){
        $product = $this->Product_Model->Get_Detail_Product()['id_product'] + 1;
        if(isset($_POST['name_product']))
            $name = $_POST['name_product'];

        if(isset($_POST['Catalogs']))
            $catalogs = json_encode($_POST['Catalogs']);
        else{
            $catalogs = json_encode([]);
        }

        if(isset($_POST['price']))
            $price = $_POST['price'];

        if(isset($_POST['sell_price']))
            $sell_price = $_POST['sell_price'];
        
        if(isset($_POST['quantity']))
            $quantity = $_POST['quantity'];
        else
            $quantity = 0;

        if(isset($_POST['Description']))
            $description = $_POST['Description'];
        
            
        if(isset($_FILES['file-input'])){               
            $target_dir = "public/images/product/details/".$product.'/';
                $fileName = $this->UpImage($target_dir);
             if($fileName != false){
                 $image = array("$fileName");
                 $image = json_encode($image);
             }else{
                $image = array();
                $image = json_encode($image);
             }
        }else {
            $image = array();
            $image = json_encode($image);
        }

        $this->Product_Model->Add_Product($name, $catalogs, $price,$sell_price,$image,$description,$quantity);
    }

    function MainCategories(){
        
        if (!parent::CheckAdmin()) return;

        $this->load->view("Admin/Admin_Header_View");
        $data = array();

        $this->load->model("Product_Model");

        if(isset($_POST['new_cate']) && !empty($_POST['new_cate'])){
            $this->Product_Model->Add_Main_Cate($_POST['new_cate']);
        }

        if(isset($_POST['cate_id']) && !empty($_POST['cate_id'])){
            $this->Product_Model->Edit_Main_Cate($_POST['cate_id'],$_POST['name_cate']);
        }

        if(isset($_GET['Del']) && !empty($_GET['Del'])){
            $this->Product_Model->Delete_Main_Cate($_GET['Del']);
        }

        $data['main_cate'] = $this->Product_Model->Get_List_Main_Cate();            
        $data['sub_cate'] = $this->Product_Model->Get_Sub_Cate();

        $this->load->view("Admin/fragments/Product/Admin_MainCate_View",$data);

        $this->load->view("Admin/Admin_Footer_View");
    }

    function SubCategories(){
        
        if (!parent::CheckAdmin()) return;

        $this->load->view("Admin/Admin_Header_View");
        $data = array();

        $this->load->model("Product_Model");

        if(isset($_POST['new_sub_cate']) && !empty($_POST['new_sub_cate'])){
            $this->Product_Model->Add_Sub_Cate($_POST['new_sub_cate'],$_POST['main_cate']);
        }

        if(isset($_POST['cate_id']) && !empty($_POST['cate_id'])){
            $this->Product_Model->Edit_Sub_Cate($_POST['cate_id'],$_POST['name_cate']);
        }

        if(isset($_GET['Del']) && !empty($_GET['Del'])){
            $this->Product_Model->Delete_Sub_Cate($_GET['Del']);
        }

        $data['sub_cate'] = $this->Product_Model->Get_Sub_Cate();

        if(isset($_GET['main']) && !empty($_GET['main'])){
            $data['sub_cate'] = $this->Product_Model->Get_Sub_Cate($_GET['main']);
            $data['current_main'] = $_GET['main'];
        }

        $data['main_cate'] = $this->Product_Model->Get_List_Main_Cate();           

        $this->load->view("Admin/fragments/Product/Admin_SubCate_View",$data);

        $this->load->view("Admin/Admin_Footer_View");
    }


    function Edit_Product(){
        if(isset($_POST['id_product']))
            $product = $this->Product_Model->Get_Detail_Product($_POST['id_product']);
        else
            $product = $this->Product_Model->Get_Detail_Product($_GET['id']);

        if(isset($_POST['name_product']))
            $name = $_POST['name_product'];
        else
            $name = $product['name'];

        if(isset($_POST['Catalogs']))
            $catalogs = json_encode($_POST['Catalogs']);
        else
            $catalogs = $product['catalogs'];

        if(isset($_POST['price']))
            $price = $_POST['price'];
        else
            $price = $product['price'];

        if(isset($_POST['sell_price']))
            $sell_price = $_POST['sell_price'];
        else
            $sell_price = $product['sell_price'];

        if(isset($_POST['quantity']))
            $quantity = $_POST['quantity'];
        else
            $quantity = $product['Quantity'];

        if(isset($_POST['Description']))
            $description = $_POST['Description'];
        else
            $description = $product['description'];
        
            
        if(isset($_FILES['file-input'])){               
            $target_dir = "public/images/product/details/".$product['id_product'].'/';
            $fileName = $this->UpImage($target_dir);
             if($fileName != false){
                 $image = json_decode($product['image']);
                 array_push($image,$fileName);
                 $image = json_encode($image);
             }else{
                 $image = $product['image'];
             }
        }else if(isset($_GET['img']) && !empty($_GET['img']) ){
            $image = (array)json_decode($product['image']);
            $key = array_search($_GET['img'],$image);
            if($key!==false){
                unset($image[$key]);
                unlink('public/images/product/details/'.$product['id_product'].'/'.$_GET['img']);
            }
            $image = json_encode(array_values($image));
        }else
            $image = $product['image'];

        $this->Product_Model->Update_Product($product['id_product'],$name, $catalogs, $price,$sell_price,$image,$description,$quantity);

    }

    function UpImage( $target_dir = ''){
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }
        $fileName = basename($_FILES["file-input"]["name"]);
        $target_file = $target_dir . $fileName;
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["file-input"]["tmp_name"]);
            if($check !== false) {
                var_dump( "File is an image - " . $check["mime"] . ".");
                $uploadOk = 1;
            } else {
                var_dump( "File is not an image.");
                $uploadOk = 0;
                return false;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            $fileName = $this->generateRandomString(2).'.'.$imageFileType;
            $target_file = $target_dir. $fileName;
            $uploadOk = 1;
        }

        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            var_dump( "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
            $uploadOk = 0;
            return false;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            var_dump( "Sorry, your file was not uploaded.");
            return false;
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["file-input"]["tmp_name"], $target_file)) {
                //var_dump( "The file ". $fileName. " has been uploaded.");
                return $fileName;
            } else {
                var_dump( "Sorry, there was an error uploading your file.");
                return false;
            }
        }

    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            return false;
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
        return true;
    }
}

?>