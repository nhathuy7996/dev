<?php
class User_Controller extends MY_Controller{
    function index(){
        $data = array();
        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();

        $this->load->model('Users_Model');
        if( parent::CheckUser()){
            $data['userdata'] =  $this->Users_Model->Get_user_data($_SESSION['userid']);
        }

        $this->load->view('User_View',$data);
    }
    function SignUp($errors = ''){
        $data = array();
        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();
        $this->load->model('Users_Model');

        if(parent::CheckUser()){
            $this->index();
            return;
        }
        $data['errors'] = $errors;

        $this->load->view('SignUp_View',$data);
    }

    function SignUp_verify(){

        if(isset($_POST['code'])){
            $this->Acc_Verify();
            return;
        }
            
        $errors = array();
        $this->load->model('Users_Model');

        if(empty($_POST['username']))
            array_push($errors,'Username empty!');

        if(isset($_POST['username']) && !$this->Users_Model->Check_Username($_POST['username']))
            array_push($errors,'Username already exist!');

        if(empty($_POST['phone']))
            array_push($errors,'Phone number empty!');

        if(isset($_POST['phone']) && !$this->Users_Model->Check_Phone($_POST['phone']))
            array_push($errors,'Phone number already exist!');

        if(isset($_POST['password']) && isset($_POST['cf_pass']) )
            if($_POST['password'] != $_POST['cf_pass'])
                array_push($errors,"Confirm pass doesn't match!");
        
        if(!empty($errors))
            $this->SignUp($errors);
        else{
            $this->Users_Model->SignUP($_POST['username'],$_POST['password'], $_POST['phone']);
            $this->index();
        }
    }

    function Acc_Verify(){
        error_reporting(0);
        define( "FB_ACCOUNT_KIT_APP_ID", "1961581170794249" );
        define( "FB_ACCOUNT_KIT_APP_SECRET", "0a26a7df05b3f7705dcea936fc9c27ff" );
        $code = $_POST['code'];
        $csrf = $_POST['csrf'];
        $auth = file_get_contents( 'https://graph.accountkit.com/v1.1/access_token?grant_type=authorization_code&code='.  $code .'&access_token=AA|'. FB_ACCOUNT_KIT_APP_ID .'|'. FB_ACCOUNT_KIT_APP_SECRET );
        $access = json_decode( $auth, true );
        if( empty( $access ) || !isset( $access['access_token'] ) ){
            return array( "status" => 2, "message" => "Unable to verify the phone number." );
        }
        //App scret proof key Ref : https://developers.facebook.com/docs/graph-api/securing-requests
        $appsecret_proof= hash_hmac( 'sha256', $access['access_token'], FB_ACCOUNT_KIT_APP_SECRET ); 
        //echo 'https://graph.accountkit.com/v1.1/me/?access_token='. $access['access_token'];
        $ch = curl_init();
        // Set query data here with the URL
        curl_setopt($ch, CURLOPT_URL, 'https://graph.accountkit.com/v1.1/me/?access_token='. $access['access_token'].'&appsecret_proof='. $appsecret_proof ); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_TIMEOUT, '4');
        $resp = trim(curl_exec($ch));
        curl_close($ch);
        $info = json_decode( $resp, true );
        if( empty( $info ) || !isset( $info['phone'] ) || isset( $info['error'] ) ){
            return array( "status" => 2, "message" => "Unable to verify the phone number." );
        }
        $phoneNumber = $info['phone']['national_number'];
        echo json_encode( $info );
        /*
        $user = $this->db->query( "SELECT * FROM user WHERE phone_number = '". $phoneNumber ."'" )->result_array();
        if( !empty( $user ) ){
            //Create session
            return array( "status" => "01", "message" => "Login success", "token" => $jwt );
        }else{
            return array( "status" => "02", "message" => "Phonenumber not registered with us." );
        }*/
    }

    function Data_Update(){
        $this->load->model('Users_Model');
        if(!parent::CheckUser()){
            $this->index();
            return;
        }
        $userdata = $this->Users_Model->Get_user_data($_SESSION['userid']);
       
        if(isset($_POST['firstname']))
            $firstname = $_POST['firstname'];
        else
            $firstname = $userdata['firstname'];

        if(isset($_POST['lastname']))
            $lastname = $_POST['lastname'];
        else
            $lastname = $userdata['lastname'];
        
        if(isset($_POST['phone']))
            $phone = $_POST['phone'];
        else
            $phone = $userdata['phone'];
        
        if(isset($_POST['address']))
            $address = $_POST['address'];
        else
            $address = $userdata['address'];

        if(isset($_POST['address2']))
            $address2 = $_POST['address2'];
        else
            $address2 = $userdata['address2'];
        
        $avatar = $userdata['avatar'];
        if(isset($_FILES['avatar']) && !empty($_FILES['avatar']["name"])){
            if(!empty($avatar))
                unlink('public/images/avatars/'.$avatar);
            $avatar = $this->UpImage();
            if($avatar == false) $IconImage = '';
        }

        $this->Users_Model->Update_User_data($_SESSION['userid'],$avatar ,$firstname, $lastname,$address,$address2,$phone);
        $this->index();
    }

    function Pass_Update(){
        $this->load->model('Users_Model');
        if(!parent::CheckUser()){
            $this->index();
            return;
        }

        $userdata = $this->Users_Model->Get_user_data($_SESSION['userid']);
        $username = $userdata['username'];

        if($this->Users_Model->CheckUser($username,$_POST['pass']) != $_SESSION['userid'] ){
            $this->index();
            return;
        }
        
        if($_POST['new_pass'] != $_POST['cf_new_pass']){
            $this->index();
            return;
        }

        $this->Users_Model->Update_User_pass($_SESSION['userid'],$_POST['new_pass']);    
        $this->index();
    }

    function Logout(){
        unset($_SESSION['userid']);
        unset($_SESSION['IsAdmin']);
        $this->load->model('Home_Model');
        $data['link_slide'] = $this->Home_Model->Get_Slider();
        $data['link_promo'] = $this->Home_Model->Get_Promo();

        $data['post'] = $this->Home_Model->Get_Blog();

        $this->load->model('Product_Model');
        $data['new'] =  $this->Product_Model->Get_NewProduct();

        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();

        $this->load->view('Home_View',$data);
    }

    function UpImage(){
        $target_dir = "public/images/avatars/";
        $fileName = basename($_FILES['avatar']["name"]);
        $target_file = $target_dir . $fileName;
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES['avatar']["tmp_name"]);
            if($check !== false) {
                var_dump( "File is an image - " . $check["mime"] . ".");
                $uploadOk = 1;
            } else {
                var_dump( "File is not an image.");
                $uploadOk = 0;
                return false;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            $fileName = $this->generateRandomString(2).'.'.$imageFileType;
            $target_file = $target_dir. $fileName;
            $uploadOk = 1;
        }
  
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            var_dump( "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
            $uploadOk = 0;
            return false;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            var_dump( "Sorry, your file was not uploaded.");
            return false;
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES['avatar']["tmp_name"], $target_file)) {
                //var_dump( "The file ". $fileName. " has been uploaded.");
                return $fileName;
            } else {
                var_dump( "Sorry, there was an error uploading your file.");
                return false;
            }
        }
  
    }
  
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
?>