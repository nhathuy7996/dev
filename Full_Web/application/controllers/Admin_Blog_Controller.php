<?php 
class Admin_Blog_Controller extends MY_Controller{
    function Blog($Theme_id = '',$page = 1){
        if (!parent::CheckAdmin()) return;

        $data = array();
        $this->load->view("Admin/Admin_Header_View");

        $this->load->model('Blog_Model');      

        if(isset($_POST['id-post']) && !empty($_POST['id-post']))
            $this->Edit_Post();
        
        
        if(isset($_GET['Del'])){
            $this->Blog_Model->Delete_Post($_GET['Del']);
        }


        $data['theme'] = $this->Blog_Model->Get_Theme_Blog($Theme_id);
        $data['post'] = $this->Blog_Model->Get_List_Post($Theme_id,$page);
        $data['pages_number'] = $this->Blog_Model->Count_Page($Theme_id);
        $data['page'] = $page; 

        if(isset($_GET['id'])){
            $data['post'] = $this->Blog_Model->Get_Post($_GET['id']);
            $data['blogs_theme'] = $this->Blog_Model->Get_All_Theme();
            $this->load->view("Admin/fragments/Admin_BlogDetail_View",$data);
        }else  if(isset($_GET['Add'])){
            $this->Add_Post();
            $data['blogs_theme'] = $this->Blog_Model->Get_All_Theme();
            $this->load->view("Admin/fragments/Admin_BlogDetail_View",$data);
        } else
            $this->load->view("Admin/fragments/Admin_Blog_View",$data);
        
        $this->load->view("Admin/Admin_Footer_View");
    }

    function Edit_Post(){
        $post = $this->Blog_Model->Get_Post($_POST['id-post']);
        if(isset($_FILES['file-input'])){               
            unlink('public/images/blog-post/'.$post['picture']);
            $target_dir = "public/images/blog-post/";
            $this->UpImage($post['picture'],$target_dir);
        }
            
        
        if(isset($_POST['title']))
            $title = $_POST['title'];
        else
            $title = $post['title'];
        
        if(isset($_POST['summary']))
            $summary = $_POST['summary'];
        else
            $summary = $post['summary'];

        if(isset($_POST['author']))
            $author = $_POST['author'];
        else
            $author = $post['author'];
        
        if(isset($_POST['content-blog']))
            $content = $_POST['content-blog'];
        else
            $content = $post['content-blog'];

        if(isset($_POST['theme']))
            $theme = json_encode($_POST['theme']);
        else
            $theme = $post['theme'];

        $this->Blog_Model->Update_Post($post['id_post'],$title,$summary,$author,$theme,$content);

    }

    function Add_Post(){
        if(isset($_FILES['file-input'])){               
            $target_dir = "public/images/blog-post/";
            $this->UpImage($_FILES['file-input']["name"],$target_dir);
        
                    
        if(isset($_POST['title']))
        $title = $_POST['title'];
    
        if(isset($_POST['summary']))
            $summary = $_POST['summary'];

        if(isset($_POST['author']))
            $author = $_POST['author'];
        
        if(isset($_POST['content-blog']))
            $content = $_POST['content-blog'];

        if(isset($_POST['theme']))
            $theme = json_encode($_POST['theme']);

        $this->Blog_Model->Add_Post($_FILES['file-input']["name"],$title,$summary,$author,$theme,$content);

        header('Location: '.BASE_URL().'Blog');
        }
    }

    function Theme(){
        if (!parent::CheckAdmin()) return;

        $data = array();
        $this->load->view("Admin/Admin_Header_View");

        $this->load->model('Blog_Model'); 
        
        if(isset($_POST['new_theme']) && !empty($_POST['new_theme'])){
            $this->Blog_Model->Add_Theme($_POST['new_theme']);
        }

        if(isset($_POST['id_theme']) && !empty($_POST['id_theme'])){
            $this->Blog_Model->Edit_Theme($_POST['id_theme'],$_POST['name_theme']);
        }

        if(isset($_GET['Del']) && !empty($_GET['Del'])){
            $this->Blog_Model->Delete_Theme($_GET['Del']);
        }

        
        $data['theme'] = $this->Blog_Model->Get_All_Theme();

        $this->load->view("Admin/fragments/Admin_BlogTheme_View",$data);
        
        $this->load->view("Admin/Admin_Footer_View");

    }

    function UpImage($old_image = '' , $target_dir = ''){
        $target_file = $target_dir . basename($old_image);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["file-input"]["tmp_name"]);
            if($check !== false) {
                var_dump( "File is an image - " . $check["mime"] . ".");
                $uploadOk = 1;
            } else {
                var_dump( "File is not an image.");
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            var_dump( "Sorry, file already exists.");
            $uploadOk = 0;
        }

        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            var_dump( "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            var_dump( "Sorry, your file was not uploaded.");
            return false;
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["file-input"]["tmp_name"], $target_file)) {
                var_dump( "The file ". basename( $_FILES["file-input"]["name"]). " has been uploaded.");
                return true;
            } else {
                var_dump( "Sorry, there was an error uploading your file.");
                return false;
            }
        }

    }
}
?>