<?php
class Cart_Controller extends CI_Controller {
    public function index(){
        $data = array();

        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();

        if(isset($_SESSION['cart']) && !empty($_SESSION['cart']))
            $data['cart'] = (array)json_decode($_SESSION['cart']);
        else
            $data['cart'] = '';

        if(isset($_GET['id'])){
           foreach($data['cart'] as $c){
                if($c->id_product == $_GET['id']){
                    $key = array_search($c,$data['cart']);
                    if($key!==false){
                        unset($data['cart'][$key]);
                    }
                }
           }
           $_SESSION['cart'] = json_encode($data['cart']);
        }

        if(isset($_POST['cart'])){
            foreach($data['cart'] as $c){
                $this->Edit_Product($c->id_product,$c->quantity);
           }
            $this->Orders();
        }

        $this->load->model('Setting_Model');
        $data['ship'] = $this->Setting_Model->Get_Information()['shipping'];
        $data['setting'] = $this->Setting_Model->Get_all_setting();

        $this->load->view('Cart_View',$data);
    }

    function Edit_Product($id,$quantity){
        $product = $this->Product_Model->Get_Detail_Product($id);

        $name = $product['name'];
        $catalogs = $product['catalogs'];
        $price = $product['price'];
        $sell_price = $product['sell_price'];
        $quantity = $product['Quantity'] - $quantity;
        $description = $product['description'];
        $image = $product['image'];

        $this->Product_Model->Update_Product($product['id_product'],$name, $catalogs, $price,$sell_price,$image,$description,$quantity);

    }

    public function Orders(){
        $name = $_POST['name'];
        $last_name = $_POST['lastname'];
        $compname = $_POST['compname'];
        $address1 = $_POST['address1'];
        $address2 = $_POST['address2'];
        $city = $_POST['city'];
        $note = $_POST['note'];
        $cart = $_POST['cart'];

        $this->load->model("Cart_Model");
        $this->Cart_Model->Place_Order($name,$last_name,$compname,$address1,$address2,$city,$note,$cart);
        
    }
}

?>