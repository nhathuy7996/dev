<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product_Controller extends CI_Controller{
    public function index($page = 1){
        $data = array();

        $this->load->model("Product_Model");

        $data['Products'] = $this->Product_Model->Get_List_Product('',$page);
        $data['pages_number'] = $this->Product_Model->Count_Page();
        $data['page'] = $page;
        $data['catalog'] = '';
        $data['main_cate'] = $this->Product_Model->Get_List_Main_Cate();

        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();
        
        $data['sub_cate'] = $this->Product_Model->Get_Sub_Cate();
        $this->load->view('Products_View',$data);
    }

    public function Catalog ($id_catalog = '',$page = 1){
        $data = array();

        $this->load->model("Product_Model");

        $data['Products'] = $this->Product_Model->Get_List_Product($id_catalog,$page);
        $data['pages_number'] = $this->Product_Model->Count_Page($id_catalog);
        $data['page'] = $page;
        $data['catalog'] = $id_catalog.'/';
        $data['main_cate'] = $this->Product_Model->Get_List_Main_Cate();
        
        $data['sub_cate'] = $this->Product_Model->Get_Sub_Cate();

        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();

        $this->load->view('Products_View',$data);
    }

    public function Product_Detail($Product_id = ''){
        $data = array();

        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();

        $data['Product_id'] = $Product_id;
        $this->load->model('Product_Model');

        $data['Product_detail'] = $this->Product_Model->Get_Detail_Product($Product_id);
        
        $cart = array();
       
        if(isset($_SESSION['cart'])){
            $cart = (array)json_decode($_SESSION['cart']);
        }

        if(isset($_POST['number_product'])){  
            if($data['Product_detail']['Quantity'] >= $_POST['number_product']){
                $is_in = false;
                foreach($cart as $c){
                    if($c->id_product == $data['Product_detail']['id_product']){
                        $c->quantity += $_POST['number_product'];
                        $is_in = true;
                        break;
                    }
                }
                
                if(!$is_in){
                    $data['Product_detail']['quantity'] = $_POST['number_product'];
                    array_push($cart,$data['Product_detail']);
                }
            }
        }

        $_SESSION['cart'] = json_encode($cart);
             
        $this->load->view('Product_Detail_View',$data);
    }

    public function Order(){
        $this->load->view('Order_View');
    }
}

?>