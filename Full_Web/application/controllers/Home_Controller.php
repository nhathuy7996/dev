<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home_Controller extends CI_Controller{

    public function index(){
        $data = array();
       

        $this->load->model('Home_Model');
        $data['link_slide'] = $this->Home_Model->Get_Slider();
        $data['link_promo'] = $this->Home_Model->Get_Promo();
        $data['partner_logo'] = $this->Home_Model->Get_Partner_Image();

        $data['post'] = $this->Home_Model->Get_Blog();

        $this->load->model('Product_Model');
        $data['new'] =  $this->Product_Model->Get_NewProduct();

        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();

        $this->load->view('Home_View',$data);
    }

    public function Contact(){
        $data = array();
        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();
        $data['info'] = $this->Setting_Model->Get_Information();

        $this->load->view('Contact_View',$data);
    }
    public function Policy(){
        $data = array();
        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();
        $data['info'] = $this->Setting_Model->Get_Information();

        $this->load->view('Policy_View',$data);
    }

    public function Term(){
        $data = array();
        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();
        $data['info'] = $this->Setting_Model->Get_Information();

        $this->load->view('Term_View',$data);
    }

    public function About(){
        $data = array();
        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();
        $data['info'] = $this->Setting_Model->Get_Information();

        $this->load->view('About_View',$data);
    }
}

?>