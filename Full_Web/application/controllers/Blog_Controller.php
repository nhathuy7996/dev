<?php
class Blog_Controller extends MY_Controller{
    function index($page = 1){
        $data = array();
        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();

        $this->load->model('Blog_Model');
        $data['post'] = $this->Blog_Model->Get_List_Post('',$page);
        $data['theme']['id'] = '';
        $data['pages_number'] = $this->Blog_Model->Count_Page();
        $data['page'] = $page;

        $this->load->view('Blog_View',$data);
    }

    function Detail($Post_id = ''){
        $data = array();
        $this->load->model('Blog_Model');
        $this->load->model('Comment_Model');
        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();


        $data['post']  = $this->Blog_Model->Get_Post($Post_id);
        $data['themes'] = $this->Blog_Model->Get_All_Theme();

        if(isset($_POST['comment']) && !empty($_POST['comment']) && $this->CheckUser() ){
            $this->Comment_Model->Add_Comment($_POST['comment'],$Post_id);
        }
        $data['comment'] = $this->Comment_Model->Get_Blog_cmt($Post_id);
 
        $this->load->view('Blog_Detail_View',$data);
    }

    function Theme($Theme_id = '',$page = 1){
        $data = array();

        $this->load->model('Setting_Model');
        $data['setting'] = $this->Setting_Model->Get_all_setting();

        $this->load->model('Blog_Model');

        $data['theme'] = $this->Blog_Model->Get_Theme_Blog($Theme_id);
        $data['post'] = $this->Blog_Model->Get_List_Post($Theme_id,$page);
        $data['pages_number'] = $this->Blog_Model->Count_Page($Theme_id);
        $data['page'] = $page;
        $this->load->view('Blog_View',$data);
    }

    public function Get_user_data($id = ''){
        $this->load->model('Users_Model');
        return $this->Users_Model->Get_user_data($id);
    }
    
    

}

?>