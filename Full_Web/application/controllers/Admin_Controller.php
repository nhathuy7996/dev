<?php 
class Admin_Controller extends MY_Controller{
    function index(){

        if (!parent::CheckAdmin()) return;


        if(isset($_GET['Logout'])){
            unset($_SESSION['IsAdmin']);
            $this->load->view("Admin/Login_View");
            return;
        }
            
        
            $this->load->view("Admin/Admin_Header_View");
            $this->load->view("Admin/fragments/Admin_Body_View");
            $this->load->view("Admin/Admin_Footer_View");
        
    }

    function Slider(){
        if (!parent::CheckAdmin()) return;

        $data = array();
        $this->load->model('Home_Model');

        if(isset($_GET['id'])){
            $this->Home_Model->Del_Slider($_GET['id']);
        }

        if(isset($_FILES['file-input'])){               
            $target_dir = "public/images/slide/";
            if($this->UpImage($_FILES['file-input']["name"],$target_dir))
                $this->Home_Model->Add_Slider($_FILES['file-input']["name"],$_POST['setting']);
        }

        $data['link_slide'] = $this->Home_Model->Get_Slider();

        $data['post'] = $this->Home_Model->Get_Blog();

        $this->load->view("Admin/Admin_Header_View");
        $this->load->view("Admin/fragments/Admin_Slider_View",$data);

        $this->load->view("Admin/Admin_Footer_View");
    }

    function Promo(){
        if (!parent::CheckAdmin()) return;

        $data = array();
        $this->load->model('Home_Model');

        if(isset($_GET['id'])){
            $this->Home_Model->Del_Promo($_GET['id']);
        }

        if(isset($_FILES['file-input'])){               
            $target_dir = "public/images/promo/";
            if($this->UpImage($_FILES['file-input']["name"],$target_dir))
                $this->Home_Model->Add_Promo($_FILES['file-input']["name"]);
        }

        $data['link_promo'] = $this->Home_Model->Get_Promo();

        $data['post'] = $this->Home_Model->Get_Blog();

        $this->load->view("Admin/Admin_Header_View");
        $this->load->view("Admin/fragments/Admin_Promo_View",$data);

        $this->load->view("Admin/Admin_Footer_View");
    }

    function Partner_Image(){
        if (!parent::CheckAdmin()) return;

        $data = array();
        $this->load->model('Home_Model');

        if(isset($_GET['id'])){
            $this->Home_Model->Del_Partner_Image($_GET['id']);
        }

        if(isset($_FILES['file-input'])){               
            $target_dir = "public/images/partner/";
            if($this->UpImage($_FILES['file-input']["name"],$target_dir))
                $this->Home_Model->Add_Partner_Image($_FILES['file-input']["name"]);
        }

        $data['link_promo'] = $this->Home_Model->Get_Partner_Image();

        $data['post'] = $this->Home_Model->Get_Blog();

        $this->load->view("Admin/Admin_Header_View");
        $this->load->view("Admin/fragments/Admin_PartnerImage_View",$data);

        $this->load->view("Admin/Admin_Footer_View");
    }

    function UpImage($old_image = '' , $target_dir = ''){
        $target_file = $target_dir . basename($old_image);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["file-input"]["tmp_name"]);
            if($check !== false) {
                var_dump( "File is an image - " . $check["mime"] . ".");
                $uploadOk = 1;
            } else {
                var_dump( "File is not an image.");
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            var_dump( "Sorry, file already exists.");
            $uploadOk = 0;
        }

        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            var_dump( "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            var_dump( "Sorry, your file was not uploaded.");
            return false;
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["file-input"]["tmp_name"], $target_file)) {
                var_dump( "The file ". basename( $_FILES["file-input"]["name"]). " has been uploaded.");
                return true;
            } else {
                var_dump( "Sorry, there was an error uploading your file.");
                return false;
            }
        }

    }

    
}

?>