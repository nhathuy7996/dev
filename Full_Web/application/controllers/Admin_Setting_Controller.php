<?php

class Admin_Setting_Controller extends MY_Controller{

    function index(){
      $data = array();

      if (!parent::CheckAdmin()) return;

      $this->load->model('Setting_Model');

      if(isset($_POST['Edit']))
        $this->UpdateSetting();

      $data['setting'] = $this->Setting_Model->Get_all_setting();

      $this->load->view('Admin/Admin_Header_View');
      $this->load->view('Admin/fragments/Setting/Admin_MainSetting_View',$data);
      $this->load->view('Admin/Admin_Footer_View');
    }

    function Logo(){
      $data = array();

      if (!parent::CheckAdmin()) return;

      $this->load->model('Setting_Model');

      if(isset($_POST['Edit']))
        $this->UpdateSetting();

      $data['setting'] = $this->Setting_Model->Get_all_setting();

      $this->load->view('Admin/Admin_Header_View');
      $this->load->view('Admin/fragments/Setting/Admin_LogoSetting_View',$data);
      $this->load->view('Admin/Admin_Footer_View');
    }

    function UpdateSetting(){
      $setting = $this->Setting_Model->Get_all_setting();

      if(isset($_POST['name_web']))
        $NameWeb = $_POST['name_web'];
      else
        $NameWeb = $setting['name_web'];

      if(isset($_POST['slogan']))
        $Slogan = $_POST['slogan'];
      else
        $Slogan = $setting['slogan'];

      if(isset($_POST['intro']))
        $Intro = $_POST['intro'];
      else
        $Intro = $setting['intro'];

      if(isset($_POST['language']))
        $Language = $_POST['language'];
      else
        $Language = $setting['language'];

        $IconImage = $setting['icon'];
      if(isset($_FILES['icon_image']) && !empty($_FILES['icon_image']["name"])){
        if(!empty($IconImage))
          unlink('public/images/'.$IconImage);
        $IconImage = $this->UpImage('icon_image');
        if($IconImage == false) $IconImage = '';
      }

      $IntroImage = $setting['intro_image'];
      if(isset($_FILES['image_intro']) && !empty($_FILES['image_intro']["name"])){
        if(!empty($IntroImage))
          unlink('public/images/'.$IntroImage);
        $IntroImage = $this->UpImage('image_intro');
        if($IntroImage == false) $IntroImage = '';
      }

      $Logo = $setting['logo'];
      if(isset($_FILES['logo_image']) && !empty($_FILES['logo_image']["name"])){
        if(!empty($Logo))
          unlink('public/images/'.$Logo);
        $Logo = $this->UpImage('logo_image');
        if($Logo == false) $Logo = '';
      }

      $this->Setting_Model->Update_Setting($NameWeb,$Slogan,$Intro,$Language,$IconImage ,$IntroImage,$Logo);

    }

    function Infomation(){
      $data = array();

      if (!parent::CheckAdmin()) return;

      $this->load->model('Setting_Model');

      if(isset($_POST['EditInfo']))
        $this->UpdateInfo();

      $data['info'] = $this->Setting_Model->Get_Information();

      $this->load->view('Admin/Admin_Header_View');
      $this->load->view('Admin/fragments/Setting/Admin_Infomation_View',$data);
      $this->load->view('Admin/Admin_Footer_View');
    }

    function Condition(){
      $data = array();

      if (!parent::CheckAdmin()) return;

      $this->load->model('Setting_Model');

      if(isset($_POST['EditInfo']))
        $this->UpdateInfo();

      $data['info'] = $this->Setting_Model->Get_Information();

      $this->load->view('Admin/Admin_Header_View');
      $this->load->view('Admin/fragments/Setting/Admin_Condition_View',$data);
      $this->load->view('Admin/Admin_Footer_View');
    }

    function UPdateInfo(){
      $info = $this->Setting_Model->Get_Information();
      if(isset($_POST['address']))
        $address = $_POST['address'];
      else
        $address = $info['address'];

      if(isset($_POST['phone']))
        $phone = $_POST['phone'];
      else
        $phone = $info['phone'];

      if(isset($_POST['email']))
        $email = $_POST['email'];
      else
        $email = $info['email'];

      if(isset($_POST['shipping']))
        $shipping = $_POST['shipping'];
      else
        $shipping = $info['shipping'];

      if(isset($_POST['policy']))
        $policy = $_POST['policy'];
      else
        $policy = $info['policy'];

      if(isset($_POST['term']))
        $term = $_POST['term'];
      else
        $term = $info['term'];

      if(isset($_POST['about']))
        $about = $_POST['about'];
      else
        $about = $info['about'];

      $this->Setting_Model->UPdateInfo($address,$phone,$email,$policy,$term,$shipping,$about);

    }

  function UpImage($id=''){
      $target_dir = "public/images/";
      $fileName = basename($_FILES[$id]["name"]);
      $target_file = $target_dir . $fileName;
      $uploadOk = 1;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
      // Check if image file is a actual image or fake image
      if(isset($_POST["submit"])) {
          $check = getimagesize($_FILES[$id]["tmp_name"]);
          if($check !== false) {
              var_dump( "File is an image - " . $check["mime"] . ".");
              $uploadOk = 1;
          } else {
              var_dump( "File is not an image.");
              $uploadOk = 0;
              return false;
          }
      }
      // Check if file already exists
      if (file_exists($target_file)) {
          $fileName = $this->generateRandomString(2).'.'.$imageFileType;
          $target_file = $target_dir. $fileName;
          $uploadOk = 1;
      }

      // Allow certain file formats
      if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
      && $imageFileType != "gif" ) {
          var_dump( "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
          $uploadOk = 0;
          return false;
      }
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
          var_dump( "Sorry, your file was not uploaded.");
          return false;
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES[$id]["tmp_name"], $target_file)) {
              //var_dump( "The file ". $fileName. " has been uploaded.");
              return $fileName;
          } else {
              var_dump( "Sorry, there was an error uploading your file.");
              return false;
          }
      }

  }

  function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }
   

}

?>