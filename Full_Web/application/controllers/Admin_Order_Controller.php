<?php
class Admin_Order_Controller extends MY_Controller{
    function index(){

        if (!parent::CheckAdmin()) return;

        $this->load->view("Admin/Admin_Header_View");

        $data = array();

        if(isset($_POST['id_done'])){
            $this->SetDone();
        }

        if(isset($_POST['id_Delivery'])){
            $this->SetDelivery();
        }

        $this->load->model("Order_Model");
        $data['pending'] = $this->Order_Model->Get_Order_pending();

        $data['orders'] = $this->Order_Model->Get_All_Orders();

        $this->load->view("Admin/fragments/Order/Admin_Order_View",$data);
        $this->load->view("Admin/Admin_Footer_View");
    }


    function SetDone(){
        if(empty($_POST['id_done']))
            return;
        $this->load->model("Order_Model");
        $this->Order_Model->SetOrder_Status($_POST['id_done'],"done");

    }

    function SetDelivery(){
        if(empty($_POST['id_Delivery']))
            return;
        $this->load->model("Order_Model");
        $this->Order_Model->SetOrder_Status($_POST['id_Delivery'],"delivery");

    }
}

?>