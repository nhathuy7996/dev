-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 22, 2019 lúc 03:12 PM
-- Phiên bản máy phục vụ: 10.4.6-MariaDB
-- Phiên bản PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `ling`
--
CREATE DATABASE IF NOT EXISTS `ling` DEFAULT CHARACTER SET utf8 COLLATE utf8_vietnamese_ci;
USE `ling`;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `blog_theme`
--

CREATE TABLE `blog_theme` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `blog_theme`
--

INSERT INTO `blog_theme` (`id`, `name`) VALUES
(1, 'Actraction'),
(5, 'Level'),
(6, 'Your Way');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comment_post`
--

CREATE TABLE `comment_post` (
  `id_cmt` int(100) NOT NULL,
  `content` varchar(200) COLLATE utf8_vietnamese_ci NOT NULL,
  `user_id` int(100) NOT NULL,
  `post_id` int(100) NOT NULL,
  `date_create` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `comment_post`
--

INSERT INTO `comment_post` (`id_cmt`, `content`, `user_id`, `post_id`, `date_create`) VALUES
(2, 'hh', 2, 8, '0000-00-00'),
(3, 'vvvv', 2, 8, '0000-00-00'),
(4, 'kkk', 2, 8, '0000-00-00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comment_product`
--

CREATE TABLE `comment_product` (
  `id_cmt` int(100) NOT NULL,
  `content` varchar(200) COLLATE utf8_vietnamese_ci NOT NULL,
  `user_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `html_pages`
--

CREATE TABLE `html_pages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `content` mediumtext COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `html_pages`
--

INSERT INTO `html_pages` (`id`, `name`, `content`) VALUES
(1, 'WineWeb', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `information`
--

CREATE TABLE `information` (
  `id` int(11) NOT NULL,
  `address` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_vietnamese_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `policy` varchar(500) COLLATE utf8_vietnamese_ci NOT NULL,
  `term` varchar(500) COLLATE utf8_vietnamese_ci NOT NULL,
  `shipping` varchar(500) COLLATE utf8_vietnamese_ci NOT NULL,
  `about` varchar(500) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `information`
--

INSERT INTO `information` (`id`, `address`, `phone`, `email`, `policy`, `term`, `shipping`, `about`) VALUES
(1, 'Hà Nội', '0334771774', 'minh.hang.301198@gmail.com', '<p>- Account security</p>\r\n\r\n<p>- Order security</p>\r\n\r\n<p>- Address security</p>\r\n\r\n<p>-&nbsp;Privacy of personal information</p>\r\n', '<p>term &amp; condition here</p>\r\n', '40.000', '<p>Ship code with wines under 800,000 VND.</p>\r\n\r\n<p>For wines is&nbsp;over 800,000 VND, a 50% deposit is required.</p>\r\n');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `main_categories`
--

CREATE TABLE `main_categories` (
  `id_cate` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `main_categories`
--

INSERT INTO `main_categories` (`id_cate`, `name`) VALUES
(3, 'Chivas'),
(10, 'Johnnie Walker'),
(13, 'Fruit Wine'),
(14, 'DaLat Wine');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id_order` int(11) NOT NULL,
  `user_id` int(100) NOT NULL,
  `name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `comp_name` varchar(50) NOT NULL,
  `address1` varchar(500) NOT NULL,
  `address2` varchar(500) NOT NULL,
  `city` varchar(10) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `cart` varchar(1000) NOT NULL,
  `date_create` date NOT NULL,
  `status_order` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id_order`, `user_id`, `name`, `last_name`, `comp_name`, `address1`, `address2`, `city`, `note`, `cart`, `date_create`, `status_order`) VALUES
(2, 21, 'Nguyễn Minh Hằng', 'Hằng', '0334771774', '30', '', 'Hà Nội', '', '[{\"id_product\":\"27\",\"name\":\"French Wine Tertre DuMoulin Grand Cru\",\"catalogs\":\"[]\",\"price\":\"1500\",\"sell_price\":\"2000\",\"image\":\"[\\\"TertreDuMoulinGrandCru.png\\\"]\",\"quantity\":\"1\"}]', '2019-11-21', 'pending'),
(3, 21, 'Nguyễn Minh Hằng', 'Hằng', '', '30', '', 'Hà Nội', '', '[{\"id_product\":\"27\",\"name\":\"French Wine Tertre DuMoulin Grand Cru\",\"catalogs\":\"[]\",\"price\":\"1500\",\"sell_price\":\"2000\",\"image\":\"[\\\"TertreDuMoulinGrandCru.png\\\"]\",\"quantity\":\"1\"}]', '2019-11-21', 'pending'),
(4, 21, 'Nguyễn Minh Hằng', 'Hằng', '', '30', '', 'Hà Nội', '', '[{\"id_product\":\"27\",\"name\":\"French Wine Tertre DuMoulin Grand Cru\",\"catalogs\":\"[\\\"5\\\"]\",\"price\":\"1500\",\"sell_price\":\"2000\",\"image\":\"[\\\"TertreDuMoulinGrandCru.png\\\"]\",\"quantity\":\"1\"}]', '2019-11-21', 'pending'),
(5, 21, 'Nguyễn Minh Hằng', 'Hằng', '', '30', '', 'Hà Nội', '', '[{\"id_product\":\"27\",\"name\":\"French Wine Tertre DuMoulin Grand Cru\",\"catalogs\":\"[\\\"5\\\"]\",\"price\":\"1500\",\"sell_price\":\"2000\",\"image\":\"[\\\"TertreDuMoulinGrandCru.png\\\"]\",\"quantity\":\"1\"}]', '2019-11-21', 'pending'),
(6, 21, 'Nguyễn Minh Hằng', 'Hằng', '', '30', '', 'Hà Nội', '', '[{\"id_product\":\"26\",\"name\":\"French Wine Louis Eschenauer Sauvignon Blanc\",\"catalogs\":\"[]\",\"price\":\"1500\",\"sell_price\":\"2000\",\"image\":\"[\\\"iD.png\\\"]\",\"quantity\":\"1\"}]', '2019-11-21', 'pending');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `partner_image`
--

CREATE TABLE `partner_image` (
  `id` int(11) NOT NULL,
  `link` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `alt` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `partner_image`
--

INSERT INTO `partner_image` (`id`, `link`, `alt`) VALUES
(22, '5b61da621900002b00c6b55a.jpeg', '5b61da621900002b00c6b55a.jpeg'),
(31, 'adobestock_59574152-2048x1365(2).jpeg', 'adobestock_59574152-2048x1365(2).jpeg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post`
--

CREATE TABLE `post` (
  `id_post` int(11) NOT NULL,
  `picture` varchar(1000) NOT NULL,
  `title` varchar(100) NOT NULL,
  `summary` varchar(100) NOT NULL,
  `author` varchar(30) NOT NULL,
  `theme` varchar(100) NOT NULL,
  `content` varchar(10000) NOT NULL,
  `date_create` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `post`
--

INSERT INTO `post` (`id_post`, `picture`, `title`, `summary`, `author`, `theme`, `content`, `date_create`) VALUES
(8, '98d0fcc93720bd131e307fb6511324b8fb3a426559b4d-cZ2LoU.jpg', 'Alcohol creates attraction for men', ' Want to understand a man, look at how he drinks wine.', 'minhhang.3011', '[\"1\"]', '<p>&nbsp;</p>\r\n\r\n<p>Alcohol is also the drink to show the attraction of man. Depending on the type of alcohol he drinks, you can guess the personality of the man.&nbsp;Whether he is a grown-up man, an experienced man or just a young boy who is drinking. Is an affectionate man or a new curious man. Is an ambitious man or a man who has no difficulty in life. Or simply, whether a man has a good drink or not.</p>\r\n', '0000-00-00'),
(9, 'cognac.jpg', ' Wine contains a different level', 'High or low level depends on the person who drinks the wine.', 'minhhang.3011', '[\"5\"]', '<pre>\r\nWine is a drink containing many different levels. High or low level depends on the person who drinks the wine. There are no shops selling all kinds of wine in the world, and nowhere else is there a whole range of wines from low to high. If you want to find the same class of wine, you have to try yourself. Because it deserves!</pre>\r\n', '2019-10-21'),
(10, 'ruou-1516977874437843261833.jpg', ' Drink your own way', ' Whatever way you want to drink, just drink like that!', 'minhhang.3011', '[\"6\"]', '<pre>\r\nDrinking alcohol is not only a way to relieve mood or reduce sadness, but also a hobby for many people. The more conditional, the more people want to enjoy more types of wine with many different origins and different ways of drinking. However, no drinking method is the default for any kind of deer. It depends on the personality and preferences of the person enjoying it. So, whatever you want to drink, just drink it that way.</pre>\r\n', '2019-10-21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id_product` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `catalogs` varchar(1000) NOT NULL,
  `price` int(11) NOT NULL,
  `sell_price` int(11) NOT NULL,
  `image` varchar(10000) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `Quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id_product`, `name`, `catalogs`, `price`, `sell_price`, `image`, `description`, `Quantity`) VALUES
(19, 'Grape Wine', '[\"11\"]', 150000, 250000, '[\"ruounho.jpg\"]', '<h3><span style=\"font-family:Georgia,serif\">Grape Wine<br />\r\nNatural extracts<br />\r\nDelicious<br />\r\nGood for health</span></h3>\r\n', 0),
(20, 'Johnnie Walker Gold Label', '[\"21\"]', 950000, 1200000, '[\"jwgoldlabel.jpg\"]', '<p>Capacity: 750ml<br />\r\nConcentration: 40%<br />\r\nOrigin: Scotland</p>\r\n', 0),
(21, 'Johnnie Walker Red Label', '[\"18\"]', 440000, 4900000, '[\"jwredlabel.jpg\"]', '<h3><span style=\"font-family:Georgia,serif\">Capacity: 750ml<br />\r\nConcentration: 40%<br />\r\nOrigin: Scotland</span></h3>\r\n', 0),
(22, 'Johnnie Walker Blue Label', '[\"20\"]', 3300000, 3900000, '[\"jwbluelabel.jpg\"]', '<p>Capacity: 1000ml<br />\r\nConcentration: 40%<br />\r\nOrigin: Scotland</p>\r\n', 0),
(23, 'Johnnie Walker Black Label', '[\"19\"]', 650000, 800000, '[\"jwblacklabel.jpg\"]', '<p>Capacity: 750ml<br />\r\nConcentration: 40%<br />\r\nOrigin: Scotland</p>\r\n', 0),
(24, 'Chivas Blue', '[\"18\"]', 1150000, 1400000, '[\"chivasxanh.jpg\"]', '<h3><span style=\"font-family:Georgia,serif\">Capacity: 750ml<br />\r\nConcentration: 40%<br />\r\nOrigin: Scotland</span></h3>\r\n', 0),
(25, 'Chivas 25 YO', '[\"17\"]', 5200000, 5800000, '[\"chivashainam.jpg\"]', '<h3><span style=\"font-family:Georgia,serif\">Capacity: 750ml<br />\r\nConcentration: 40%<br />\r\nWine age: 25&nbsp;years<br />\r\nClassification: Blended Scotch Whiskey<br />\r\nOrigin: Scotland<br />\r\nSpecification: 25&nbsp;bottles / carton</span></h3>\r\n', 0),
(26, 'Chivas 21 YO', '[\"16\"]', 2250000, 2400000, '[\"chivashaimot.jpg\"]', '<h3><span style=\"font-family:Georgia,serif\">Capacity: 750ml<br />\r\nConcentration: 40%<br />\r\nWine age: 21&nbsp;years<br />\r\nClassification: Blended Scotch Whiskey<br />\r\nOrigin: Scotland<br />\r\nSpecification: 21&nbsp;bottles / carton</span></h3>\r\n', 0),
(27, 'Chivas 18 YO', '[\"15\"]', 1150000, 1250000, '[\"chivasmuoitam.jpg\"]', '<h3><span style=\"font-family:Georgia,serif\">Capacity: 750ml<br />\r\nConcentration: 40%<br />\r\nWine age: 18&nbsp;years<br />\r\nClassification: Blended Scotch Whiskey<br />\r\nOrigin: Scotland<br />\r\nSpecification: 18&nbsp;bottles / carton</span></h3>\r\n', 0),
(29, 'Chivas 12 YO', '[\"14\"]', 550000, 600000, '[\"chivasmuoihai.jpg\"]', '<h3><span style=\"font-family:Georgia,serif\">Capacity: 750ml<br />\r\nConcentration: 40%<br />\r\nWine age: 12 years<br />\r\nClassification: Blended Scotch Whiskey<br />\r\nOrigin: Scotland<br />\r\nSpecification: 12 bottles / carton</span></h3>\r\n', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `promo_image`
--

CREATE TABLE `promo_image` (
  `id` int(11) NOT NULL,
  `link` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `alt` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `setting`
--

CREATE TABLE `setting` (
  `id_setting` int(11) NOT NULL,
  `name_web` varchar(20) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `slogan` varchar(200) NOT NULL,
  `intro` varchar(200) NOT NULL,
  `intro_image` varchar(100) NOT NULL,
  `language` varchar(2) NOT NULL,
  `logo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `setting`
--

INSERT INTO `setting` (`id_setting`, `name_web`, `icon`, `slogan`, `intro`, `intro_image`, `language`, `logo`) VALUES
(1, 'Dear, Wine!', '5b61da621900002b00c6b55a.jpeg', 'Drink Your Own Way!', '<p>&nbsp;</p>\r\n\r\n<h2 style=\"text-align:center\"><span style=\"color:#990000\"><span style=\"font-size:28px\"><em><span style=\"font-family:Courier New,Courier,monospace\">Best Wine!&nbsp;Best Experience!</sp', 'adobestock_59574152-2048x1365.jpeg', 'en', 'uh.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slide_image`
--

CREATE TABLE `slide_image` (
  `id_slide` int(11) NOT NULL,
  `link` varchar(100) NOT NULL,
  `alt` varchar(100) NOT NULL,
  `setting` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `slide_image`
--

INSERT INTO `slide_image` (`id_slide`, `link`, `alt`, `setting`) VALUES
(16, 'Basically-Red-Wine-02.jpg', 'Basically-Red-Wine-02.jpg', ''),
(17, 'gettyimages-484649636-red-wine1.jpg', 'gettyimages-484649636-red-wine1.jpg', ''),
(19, 'wine-2270918_960_720.jpg', 'wine-2270918_960_720.jpg', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id_sub_cate` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `id_main_cate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `sub_categories`
--

INSERT INTO `sub_categories` (`id_sub_cate`, `name`, `id_main_cate`) VALUES
(11, 'Grape Wine', 13),
(12, ' Mulberry Wine', 13),
(13, ' Apricot Wine', 13),
(14, 'Chivas 12', 3),
(15, 'Chivas 18', 3),
(16, 'Chivas 21', 3),
(17, 'Chivas 25', 3),
(18, 'Johnnie Walker Red Label', 10),
(19, 'Johnnie Walker Black Label', 10),
(20, 'Johnnie Walker Blue Label', 10),
(21, 'Johnnie Walker Gold Label', 10),
(22, 'Guava wine', 13),
(23, 'Dalat Wine Excellence', 14),
(24, 'Dalat Wine Special Merlot', 14),
(25, 'Dalat Wine Special Chardonnay', 14),
(26, 'Dalat Wine Reserve Merlot', 14);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `isadmin` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `address2` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id_user`, `username`, `pass`, `avatar`, `isadmin`, `firstname`, `lastname`, `address`, `address2`, `phone`) VALUES
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', '4d68f0db00000578-5860843-image-a-251529419548114-1529475086343231831020.jpg', 1, 'Hang', 'Nguyen', '', '', ''),
(21, 'hangnm', 'a35bcd21b22034435ca7f95ff8d5a6e9', '256fb861aeac48f211bd.jpg', 0, 'Nguyễn', 'Hằng', '', '', '0334771774');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `blog_theme`
--
ALTER TABLE `blog_theme`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `comment_post`
--
ALTER TABLE `comment_post`
  ADD PRIMARY KEY (`id_cmt`),
  ADD KEY `cmt_post` (`user_id`),
  ADD KEY `post_relate` (`post_id`);

--
-- Chỉ mục cho bảng `comment_product`
--
ALTER TABLE `comment_product`
  ADD PRIMARY KEY (`id_cmt`);

--
-- Chỉ mục cho bảng `html_pages`
--
ALTER TABLE `html_pages`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id_cate`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `orders` (`user_id`);

--
-- Chỉ mục cho bảng `partner_image`
--
ALTER TABLE `partner_image`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id_post`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id_product`);

--
-- Chỉ mục cho bảng `promo_image`
--
ALTER TABLE `promo_image`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id_setting`);

--
-- Chỉ mục cho bảng `slide_image`
--
ALTER TABLE `slide_image`
  ADD PRIMARY KEY (`id_slide`);

--
-- Chỉ mục cho bảng `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id_sub_cate`),
  ADD KEY `categories_blog` (`id_main_cate`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `blog_theme`
--
ALTER TABLE `blog_theme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `comment_post`
--
ALTER TABLE `comment_post`
  MODIFY `id_cmt` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `comment_product`
--
ALTER TABLE `comment_product`
  MODIFY `id_cmt` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `html_pages`
--
ALTER TABLE `html_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `information`
--
ALTER TABLE `information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id_cate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `partner_image`
--
ALTER TABLE `partner_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT cho bảng `post`
--
ALTER TABLE `post`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT cho bảng `promo_image`
--
ALTER TABLE `promo_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `setting`
--
ALTER TABLE `setting`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `slide_image`
--
ALTER TABLE `slide_image`
  MODIFY `id_slide` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT cho bảng `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id_sub_cate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `comment_post`
--
ALTER TABLE `comment_post`
  ADD CONSTRAINT `cmt_post` FOREIGN KEY (`user_id`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `post_relate` FOREIGN KEY (`post_id`) REFERENCES `post` (`id_post`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders` FOREIGN KEY (`user_id`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `categories_blog` FOREIGN KEY (`id_main_cate`) REFERENCES `main_categories` (`id_cate`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
